FROM maven:3.6.3-jdk-8-slim  AS MAVEN_BUILD

COPY . /app
WORKDIR /app/
RUN mvn package -DskipTests -P release

FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=MAVEN_BUILD /app/rest-app/target/rest-app-1.0-SNAPSHOT.jar /app/
ENTRYPOINT ["java", "-jar", "rest-app-1.0-SNAPSHOT.jar"]
