## SSNews App Demo 
 
### Run demo 

Option 1: Run with docker and postgres db

    cd [source-root]
    docker-compose up

Option 2: Build and run with maven and java with H2 db

    cd [source-root]
    mvn package -DskipTests -P release
    java -Dspring.profiles.active=dev -jar ./rest-app/target/rest-app-1.0-SNAPSHOT.jar
    
    
### Access application

Access with http://localhost:8080/ URL

There is only one user at start, you need to restore password to log in

Go to http://localhost:8080/auth/forgot-password

    Email: news-admin@solarsystem.com
Press restore and check console log. Demo application runs with fake mail simulator and simply writes the email to console.

Follow activation link printed in console to set password for user.

After log in app will bring database initialization screen. Please upload file `sample-data-import.xlsx` from root folder to populate data.

Once data is populated, you can try recovering other user accounts or changing current user department from `App Administrators` to `HR` or `C-Suite`. According to uploaded config current user can't send notifications.

Navigate to `Manage Users` find `System Admin` change `Department` to `C-Suite`, log out and log in again.

**NOTE**
1. If you run with option 2, additional user `admin` with password `admin` is created (can skip password recover steps)
2. To configure real mail send, update configuration at
    
    rest-app/src/main/resources/application-prod.yml

### Architecture related info

- Simple spring boot app provides only RESTful API
- Swagger Docs are available at http://localhost:8080/swagger-ui.html
- DB can be any relational (used H2 for dev and PostgreSQL for demo)
- Code static analysis is performed with PMD (requested tool Qulice looked outdated to me and not actively supported)
- Other static analysis tools configuration (Checkstyle, SpotBugs) not finalized 
- Tests written in java and BB tests with Groovy, code coverage report is available (after build) in `code-coverage/target/site/jacoco-aggregate`
- Frontend is SPA with Angular 8 (not in the best shape though due to time limitations)
- As you may already noticed, app can run in docker containers but not fine tuned for cloud

