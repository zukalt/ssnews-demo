package com.solarsystem.app.ssnews.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Department {

    @Id
    private String id;

    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "department")
    private List<User> employees;

    public final static Department SYSTEM_DEPARTMENT = new Department("SYSTEM", "App Administrators");

    public Department(String id) {
        this(id, id);
    }

    public Department(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
