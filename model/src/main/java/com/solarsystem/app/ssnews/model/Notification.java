package com.solarsystem.app.ssnews.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Notification {

    @Id
    private String id;

    private String subject;

    private String body;

    private Date createdAt;

    @ManyToOne()
    private User createdBy;

}
