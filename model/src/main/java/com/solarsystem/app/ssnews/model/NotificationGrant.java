package com.solarsystem.app.ssnews.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class NotificationGrant {

    @Id
    private String id;

    @ManyToOne
    private Department fromDepartment;

    @ManyToOne
    private Department toDepartment;

    private boolean personalNotificationsAllowed;
}
