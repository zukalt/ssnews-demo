package com.solarsystem.app.ssnews.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class User {
    @Id
    private String id;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private String fullName;

    private LocalDate birthDate;

    @ManyToOne
    private Department department;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> ignoreList;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "createdBy")
    private List<Notification> sent;

    public User(String id, String email, String fullName, Department department) {
        this.id = id;
        this.email = email;
        this.fullName = fullName;
        this.department = department;
    }
}
