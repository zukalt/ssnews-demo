package com.solarsystem.app.ssnews.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class UserCredential {

    @Id
    private String email;

    private String passwordHash;

    @Column(unique = true)
    private String passwordResetToken;

    private Date tokenValidTill;

    public UserCredential(String email) {
        this.email = email;
    }
}
