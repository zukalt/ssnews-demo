package com.solarsystem.app.ssnews.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserNotification {

    @Id
    private String id;

    @OneToOne
    private User user;

    @OneToOne
    private Notification notification;

    private boolean read;
}
