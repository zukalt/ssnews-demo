package com.solarsystem.app.ssnews.persistence.jpa;

import com.solarsystem.app.ssnews.uc.ports.rep.NotificationStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

@Configuration
public class CustomRepositoriesConfig {

    @Bean
    NotificationStore notificationStore(EntityManager entityManager) {
        return new NotificationStoreJpaImpl(entityManager);
    }
}
