package com.solarsystem.app.ssnews.persistence.jpa;



import com.solarsystem.app.ssnews.model.Department;
import com.solarsystem.app.ssnews.model.NotificationGrant;
import com.solarsystem.app.ssnews.uc.ports.rep.NotificationGrantStore;
import org.springframework.data.repository.Repository;

import java.util.Optional;

@org.springframework.stereotype.Repository
public interface NotificationGrantsStoreJpa extends NotificationGrantStore, Repository<NotificationGrant, String> {

    Optional<NotificationGrant> findByFromDepartmentAndToDepartment(Department fromDepartment, Department toDepartment);

    default Optional<NotificationGrant> findGrant(Department senderDepartment, Department department) {
        return findByFromDepartmentAndToDepartment(senderDepartment, department);
    }

}
