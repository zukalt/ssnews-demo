package com.solarsystem.app.ssnews.persistence.jpa;


import com.solarsystem.app.ssnews.model.Notification;
import com.solarsystem.app.ssnews.model.User;
import com.solarsystem.app.ssnews.model.UserNotification;
import com.solarsystem.app.ssnews.uc.common.Utils;
import com.solarsystem.app.ssnews.uc.dto.NotificationStats;
import com.solarsystem.app.ssnews.uc.ports.rep.NotificationStore;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Transactional
public class NotificationStoreJpaImpl implements NotificationStore {

    private EntityManager entityManager;

    public NotificationStoreJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    public void saveForUser(User user, Notification notification) {
        UserNotification userNotification = new UserNotification(
                Utils.randomUUID(), user, notification, false
        );
        entityManager.merge(userNotification);
    }

    public Notification save(Notification notification) {
        return entityManager.merge(notification);
    }

    public List<UserNotification> getUserNotifications(User currentUser) {
        return entityManager
                .createQuery(
                        "select n from UserNotification n" +
                                " where n.user = ?1" +
                                " ORDER BY n.notification.createdAt desc", UserNotification.class
                )
                .setParameter(1, currentUser)
                .getResultList();
    }

    public void removeUserNotification(User currentUser, String userNotificationId) {
        entityManager
                .createQuery("delete from UserNotification n where n.user = ?1 and n.id = ?2")
                .setParameter(1, currentUser)
                .setParameter(2, userNotificationId)
                .executeUpdate();
    }

    public void updateUserNotificationStatus(User currentUser, String userNotificationId, boolean read) {
        entityManager
                .createQuery("UPDATE UserNotification n SET n.read = ?1 WHERE n.user = ?2 and n.id = ?3")
                .setParameter(1, read)
                .setParameter(2, currentUser)
                .setParameter(3, userNotificationId)
                .executeUpdate();
    }

    public NotificationStats collectStatsPerDepartment() {
        return new NotificationStats(
                notificationSentPerDepartment(),
                notificationReceivedPerDepartment()
        );
    }

    private Map<String, Long> notificationSentPerDepartment() {
        return queryForStat(
                "SELECT n.createdBy.department.id, count(ALL n)" +
                        " FROM Notification n" +
                        " GROUP BY  n.createdBy.department.id");
    }

    private Map<String, Long> notificationReceivedPerDepartment() {

        return queryForStat(
                "SELECT n.user.department.id, count(all n)" +
                        " FROM UserNotification n" +
                        " GROUP BY  n.user.department.id"
        );
    }

    private Map<String, Long> queryForStat(String query) {
        return entityManager
                .createQuery(query, Object[].class)
                .getResultList()
                .stream().collect(Collectors.toMap(
                        key -> key[0].toString(),
                        value -> (Long) value[1]
                ));
    }
}
