package com.solarsystem.app.ssnews.persistence.jpa;


import com.solarsystem.app.ssnews.model.Department;
import com.solarsystem.app.ssnews.uc.ports.rep.UserDepartmentStore;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface UserDepartmentStoreJpa extends UserDepartmentStore, Repository<Department, String> {

}
