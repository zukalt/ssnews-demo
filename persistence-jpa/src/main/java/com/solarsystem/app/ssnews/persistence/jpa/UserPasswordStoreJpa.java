package com.solarsystem.app.ssnews.persistence.jpa;

import com.solarsystem.app.ssnews.model.UserCredential;
import com.solarsystem.app.ssnews.uc.ports.rep.UserPasswordStore;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface UserPasswordStoreJpa extends UserPasswordStore, Repository<UserCredential, String> {

}
