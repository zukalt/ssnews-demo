package com.solarsystem.app.ssnews.persistence.jpa;


import com.solarsystem.app.ssnews.model.User;
import com.solarsystem.app.ssnews.uc.dto.SearchResult;
import com.solarsystem.app.ssnews.uc.ports.rep.UserStore;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface UserStoreJpa extends UserStore, Repository<User, String> {

    Page<User> findAllByFullNameLikeIgnoreCaseOrderByFullNameAsc(String fullName, Pageable pageable);

    default SearchResult<User> findUsersByFullName(String nameLike, int page, int pageSize) {
        Page<User> users = findAllByFullNameLikeIgnoreCaseOrderByFullNameAsc(nameLike, PageRequest.of(page, pageSize));
        return new SearchResult<>(users.getContent(), users.getTotalElements());
    }

}
