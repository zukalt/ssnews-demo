package com.solarsystem.app.ssnews.rest.api;


import com.solarsystem.app.ssnews.uc.SystemAdministration;
import com.solarsystem.app.ssnews.uc.ex.AppBaseException;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collection;


@RestController
@Api(tags = "System Administration")
@RequestMapping("/api/system")
public class AdminController extends BaseController {


    private SystemAdministration systemAdministration;

    public AdminController(SystemAdministration systemAdministration) {
        this.systemAdministration = systemAdministration;
    }

    @PostMapping("/initDatabase")
    public Collection<String> importData(@RequestParam("file") MultipartFile file) throws AppBaseException, IOException {
        return systemAdministration.importAppDataFromExcel(file.getInputStream());
    }
}
