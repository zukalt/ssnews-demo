package com.solarsystem.app.ssnews.rest.api;


import com.solarsystem.app.ssnews.rest.api.dto.ApiErrorDto;
import com.solarsystem.app.ssnews.rest.api.dto.ChangePasswordRequest;
import com.solarsystem.app.ssnews.rest.api.dto.CreatePasswordRequest;
import com.solarsystem.app.ssnews.rest.api.dto.ResetPasswordRequest;
import com.solarsystem.app.ssnews.uc.Authentication;
import com.solarsystem.app.ssnews.uc.AuthorizationContext;
import com.solarsystem.app.ssnews.uc.dto.UserSessionDto;
import com.solarsystem.app.ssnews.uc.ex.AppBaseException;
import com.solarsystem.app.ssnews.uc.ex.NotAuthorizedException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.ok;


@RestController
@Api(tags = "Authentication")
@RequestMapping(value = "/api/auth", produces = "application/json")
public class AuthController extends BaseController {

    private Authentication authentication;

    public AuthController(Authentication authentication) {
        this.authentication = authentication;
    }

    @PostMapping("/login")
    @ApiOperation(authorizations = {},
            value = "Logs in and returns session data on success. Use return token in to access restricted APIs.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Login successful", response = UserSessionDto.class),
            @ApiResponse(code = 401, message = "Login failed", response = ApiErrorDto.class)
    })
    public ResponseEntity<UserSessionDto> passwordLogin(@RequestParam(value = "email") String email, @RequestParam("password") String password) throws NotAuthorizedException {
        return ok(authentication.passwordLogin(email, password));
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/logout")
    @ApiOperation(value = "Logs out if session is active")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Always returns OK even if there was no such session")
    })
    public void logout() {
        authentication.logout();
    }

    @GetMapping("/session")
    @ApiOperation(value = "Returns same object as in login. Used in tests to check successful session creation")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 401, message = "Session is expired or invalid")
    })
    public ResponseEntity<UserSessionDto> session() throws AppBaseException {

        return ok(AuthorizationContext.getUserSession());
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/create-password")
    @ApiOperation(value = "Used in both initial registration and forgot-password flows to set new password")
    @ApiResponses({
            @ApiResponse(code = 201, message = "User activation(or password reset) done successfully"),
            @ApiResponse(code = 409, message = "Password was not set (invalid token and or email)"),

    })
    public void createPassword(@RequestBody @Valid ResetPasswordRequest request) throws AppBaseException {
        authentication.resetPassword(request.email, request.token, request.newPassword);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/forgot-password")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Send password reset link to email"),
            @ApiResponse(code = 409, message = "Problem with sending email (impossible due to current implementation)")
    })
    @ApiOperation(authorizations = {}, value = "Password recovery")
    public void forgotPassword(@RequestBody @Valid CreatePasswordRequest request) throws AppBaseException {
        authentication.requestPasswordReset(request.email);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/change-password")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Password Changed"),
            @ApiResponse(code = 401, message = "Current session is not valid"),
            @ApiResponse(code = 404, message = "If for some reason user not found"),
            @ApiResponse(code = 403, message = "Incorrect old pass")
    })
    public void changePassword(@RequestBody @Valid ChangePasswordRequest request) throws AppBaseException {
        authentication.changePassword(request.oldPassword, request.newPassword);
    }
}
