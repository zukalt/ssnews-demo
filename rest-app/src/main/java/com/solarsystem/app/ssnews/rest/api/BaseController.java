package com.solarsystem.app.ssnews.rest.api;

import com.solarsystem.app.ssnews.rest.api.dto.ApiErrorDto;
import com.solarsystem.app.ssnews.uc.ex.AppBaseException;
import com.solarsystem.app.ssnews.uc.ex.EntityNotFoundException;
import com.solarsystem.app.ssnews.uc.ex.NotAuthorizedException;
import com.solarsystem.app.ssnews.uc.ex.UnsuccessfulOperationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import static org.springframework.http.ResponseEntity.status;

public abstract class BaseController {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ApiErrorDto handleValidationExceptions(MethodArgumentNotValidException ex) {
        StringBuffer errors = new StringBuffer();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.append(fieldName).append(": ").append(errorMessage).append("\n");
        });
        return new ApiErrorDto(HttpStatus.BAD_REQUEST.value(), errors.toString());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ServletRequestBindingException.class})
    public ApiErrorDto handleValidationExceptions(Exception ex) {
        return new ApiErrorDto(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    }

    @ExceptionHandler(UnsuccessfulOperationException.class)
    public ResponseEntity<?> unsuccessfulOperation(UnsuccessfulOperationException ex, WebRequest request) {
        return responseFromException(HttpStatus.CONFLICT, ex);
    }
    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<?> notFound(UnsuccessfulOperationException ex, WebRequest request) {
        return responseFromException(HttpStatus.NOT_FOUND, ex);
    }
    @ExceptionHandler(NotAuthorizedException.class)
    public ResponseEntity<?> forbidden(NotAuthorizedException ex, WebRequest request) {
        return responseFromException(HttpStatus.UNAUTHORIZED, ex);
    }

    private ResponseEntity<ApiErrorDto> responseFromException(HttpStatus status, AppBaseException ex) {
        return status(status).body(new ApiErrorDto(status.value(), ex.getMessage()));
    }
}
