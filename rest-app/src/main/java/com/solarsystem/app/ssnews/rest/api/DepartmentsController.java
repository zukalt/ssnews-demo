package com.solarsystem.app.ssnews.rest.api;


import com.solarsystem.app.ssnews.rest.api.dto.ApiErrorDto;
import com.solarsystem.app.ssnews.uc.UserAdministration;
import com.solarsystem.app.ssnews.uc.dto.DepartmentDto;
import com.solarsystem.app.ssnews.uc.ex.AppBaseException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@Api(tags = "Departments Management")
@RequestMapping("/api/departments")
@ApiResponses({
        @ApiResponse(code = 401, response = ApiErrorDto.class, message = "Unauthorized"),
        @ApiResponse(code = 404, response = ApiErrorDto.class, message = "Department Not found")
})
public class DepartmentsController extends BaseController {


    private UserAdministration userAdministration;

    public DepartmentsController(UserAdministration userAdministration) {
        this.userAdministration = userAdministration;
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/")
    public List<DepartmentDto> listAll() throws AppBaseException {
        return userAdministration.listAllDepartment();
    }

//    @ResponseStatus(HttpStatus.OK)
//    @GetMapping("/{id}")
//    public DepartmentDto get(@PathVariable String id) throws AppBaseException {
//        return userAdministration.getDepartment(id);
//    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/")
    public void create(@RequestBody DepartmentDto departmentDto) throws AppBaseException {
        userAdministration.addDepartment(departmentDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public DepartmentDto update(@PathVariable String id, @RequestBody DepartmentDto departmentDto)
            throws AppBaseException {
        return userAdministration.updateDepartment(id, departmentDto);
    }

}
