package com.solarsystem.app.ssnews.rest.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping({
            "/auth/*",
            "/my/*","/my",
            "/admin/**","/admin",
            "/profile"
    })
    public String index() {
        return "forward:/index.html";
    }
}
