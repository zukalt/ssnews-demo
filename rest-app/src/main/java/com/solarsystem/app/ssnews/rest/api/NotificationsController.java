package com.solarsystem.app.ssnews.rest.api;


import com.solarsystem.app.ssnews.rest.api.dto.SendNotificationRequest;
import com.solarsystem.app.ssnews.rest.api.dto.SendPersonalNotificationRequest;
import com.solarsystem.app.ssnews.uc.NotificationManagement;
import com.solarsystem.app.ssnews.uc.dto.NotificationGrantDto;
import com.solarsystem.app.ssnews.uc.dto.NotificationStats;
import com.solarsystem.app.ssnews.uc.dto.UserNotificationDto;
import com.solarsystem.app.ssnews.uc.ex.AppBaseException;
import com.solarsystem.app.ssnews.uc.ex.NotAuthorizedException;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;


@RestController
@Api(tags = "Notifications")
@RequestMapping("/api/notifications")
public class NotificationsController extends BaseController {


    private NotificationManagement notificationManagement;

    public NotificationsController(NotificationManagement notificationManagement) {
        this.notificationManagement = notificationManagement;
    }

    @GetMapping("/")
    public List<UserNotificationDto> listAll() throws AppBaseException {
        return notificationManagement.listCurrentUserNotifications();
    }
    @DeleteMapping("/{id}")
    public void removeMyNotification(@PathVariable String id) throws AppBaseException {
        notificationManagement.removeUserNotification(id);
    }
    @PutMapping("/{id}")
    public void updateMyNotificationStatus(@PathVariable String id, @RequestBody boolean status) throws AppBaseException {
        notificationManagement.updateUserNotificationStatus(id, status);
    }

    @GetMapping("/stats")
    public NotificationStats notificationStats() throws AppBaseException {
        return notificationManagement.notificationStats();
    }
    @PostMapping("/send")
    public void sendNotification(@RequestBody @Valid SendNotificationRequest req) throws AppBaseException {
        notificationManagement.sendNotification(req.toDepartments, req.subject, req.body);
    }
    @PostMapping("/send-personal")
    public void sendPersonalNotification(@RequestBody @Valid SendPersonalNotificationRequest req) throws AppBaseException {
        notificationManagement.sendPersonalNotification(req.userId, req.subject, req.body);
    }

    @GetMapping("/grants")
    public List<NotificationGrantDto> allGrants() throws AppBaseException {
        return notificationManagement.listAllGrants();
    }

    @GetMapping("/grants/my")
    public Map<String, Boolean> grants() throws NotAuthorizedException {
        return notificationManagement.listCurrentUserGrants();
    }

    @PostMapping("/grants")
    @ResponseStatus(HttpStatus.CREATED)
    public void addNewGrant(@RequestBody NotificationGrantDto grant) throws AppBaseException {
        notificationManagement.addNotificationGrant(grant);
    }

    @DeleteMapping("/grants/{id}")
    public void removeGrant(@PathVariable String id) throws AppBaseException {
        notificationManagement.removeNotificationGrantById(id);
    }
}
