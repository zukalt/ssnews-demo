package com.solarsystem.app.ssnews.rest.api;


import com.solarsystem.app.ssnews.rest.api.dto.ApiErrorDto;
import com.solarsystem.app.ssnews.rest.api.dto.CreateUserRequest;
import com.solarsystem.app.ssnews.rest.api.dto.UpdateUserRequest;
import com.solarsystem.app.ssnews.rest.api.dto.UsersSearchRequest;
import com.solarsystem.app.ssnews.uc.UserAdministration;
import com.solarsystem.app.ssnews.uc.dto.DepartmentDto;
import com.solarsystem.app.ssnews.uc.dto.SearchResult;
import com.solarsystem.app.ssnews.uc.dto.UserDto;
import com.solarsystem.app.ssnews.uc.dto.UserIdentityDto;
import com.solarsystem.app.ssnews.uc.ex.AppBaseException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;


@RestController
@Api(tags = "User Management")
@RequestMapping("/api/users")
@ApiResponses({
        @ApiResponse(code = 401, response = ApiErrorDto.class, message = "Unauthorized"),
        @ApiResponse(code = 404, response = ApiErrorDto.class, message = "User Not found")
})
public class UsersController extends BaseController {


    private UserAdministration userAdministration;

    public UsersController(UserAdministration userAdministration) {
        this.userAdministration = userAdministration;
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/")
    public List<UserIdentityDto> listAll() throws AppBaseException {
        return userAdministration.listAllUsers();
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}")
    public UserDto get(@PathVariable String id) throws AppBaseException {
        return userAdministration.getUser(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/")
    public UserIdentityDto create(@RequestBody @Valid CreateUserRequest createRequest) throws AppBaseException {
        UserDto user = new UserDto();
        user.email = createRequest.email;
        user.fullName = createRequest.fullName;
        user.department = new DepartmentDto(createRequest.department, null);
        user.birthDate = createRequest.birthDate;
        return userAdministration.addAndInviteNewUser(user);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public void update(@PathVariable String id, @RequestBody @Valid UpdateUserRequest user) throws AppBaseException {
        userAdministration.updateUserNameAndDepartment(id, user.fullName, user.department);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/my/ignoreList")
    public void updateCurrentUserIgnoreList(@RequestBody @Valid Set<String> ignoreList) throws AppBaseException {
        userAdministration.updateCurrentUserIgnoreList(ignoreList);
    }
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/my/ignoreList")
    public  Set<String>  getCurrentUserIgnoreList() throws AppBaseException {
        return userAdministration.getCurrentUserIgnoreList();
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/search")
    public SearchResult<UserIdentityDto> search(@RequestBody UsersSearchRequest request) throws AppBaseException {
        return userAdministration.searchUsers(request.nameLike, request.page, request.pageSize);
    }
}
