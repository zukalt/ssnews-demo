package com.solarsystem.app.ssnews.rest.api.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class ApiErrorDto {
    public int statusCode;
    public String message;

}
