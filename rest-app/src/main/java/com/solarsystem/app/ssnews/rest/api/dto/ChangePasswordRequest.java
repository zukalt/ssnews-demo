package com.solarsystem.app.ssnews.rest.api.dto;

import javax.validation.constraints.NotEmpty;

public class ChangePasswordRequest {
    @NotEmpty
    public String oldPassword;
    @NotEmpty
    public String newPassword;
}
