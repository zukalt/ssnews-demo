package com.solarsystem.app.ssnews.rest.api.dto;

import javax.validation.constraints.Email;

public class CreatePasswordRequest {
    @Email
    public String email;
}
