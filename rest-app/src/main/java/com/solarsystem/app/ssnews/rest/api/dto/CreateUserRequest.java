package com.solarsystem.app.ssnews.rest.api.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class CreateUserRequest {

    @NotEmpty
    public String fullName;

    @NotEmpty
    public String department;

    @Email
    public String email;

    @NotNull
    public LocalDate birthDate;
}
