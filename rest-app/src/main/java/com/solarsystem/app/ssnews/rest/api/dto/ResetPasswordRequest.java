package com.solarsystem.app.ssnews.rest.api.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class ResetPasswordRequest {
    @Email
    public String email;
    @NotEmpty
    public String token;
    @NotEmpty
    public String newPassword ;
}
