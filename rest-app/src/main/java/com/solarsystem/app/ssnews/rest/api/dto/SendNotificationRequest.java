package com.solarsystem.app.ssnews.rest.api.dto;

import javax.validation.constraints.NotEmpty;
import java.util.Set;

public class SendNotificationRequest {
    @NotEmpty
    public Set<String> toDepartments;
    @NotEmpty
    public String subject;
    @NotEmpty
    public String body;
}
