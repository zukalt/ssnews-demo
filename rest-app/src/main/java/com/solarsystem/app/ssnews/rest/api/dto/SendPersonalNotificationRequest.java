package com.solarsystem.app.ssnews.rest.api.dto;

import javax.validation.constraints.NotEmpty;

public class SendPersonalNotificationRequest {
    @NotEmpty
    public String userId;
    @NotEmpty
    public String subject;
    @NotEmpty
    public String body;
}
