package com.solarsystem.app.ssnews.rest.api.dto;

import javax.validation.constraints.NotEmpty;

public class UpdateUserRequest {

    @NotEmpty
    public String fullName;

    @NotEmpty
    public String department;
}
