package com.solarsystem.app.ssnews.rest.api.dto;

public class UsersSearchRequest {
    public String nameLike;
    public int page = 0;
    public int pageSize = 10;
}
