package com.solarsystem.app.ssnews.rest.config;


import com.solarsystem.app.ssnews.persistence.jpa.CustomRepositoriesConfig;
import com.solarsystem.app.ssnews.rest.impl.OrgDataExcelReaderImpl;
import com.solarsystem.app.ssnews.uc.*;
import com.solarsystem.app.ssnews.uc.impl.DefaultPermissionProvider;
import com.solarsystem.app.ssnews.uc.impl.FakeMailer;
import com.solarsystem.app.ssnews.uc.impl.SessionInMemoryStoreImpl;
import com.solarsystem.app.ssnews.uc.ports.AppMailer;
import com.solarsystem.app.ssnews.uc.ports.OrgDataExcelReader;
import com.solarsystem.app.ssnews.uc.ports.PasswordEncoder;
import com.solarsystem.app.ssnews.uc.ports.PermissionProvider;
import com.solarsystem.app.ssnews.uc.ports.rep.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.util.DigestUtils;

@Configuration
@Import(CustomRepositoriesConfig.class)
@EnableAsync
public class AppConfiguration {

    @Value("${app.sessions.keepAliveTimeMs}")
    private long sessionExpirationTimeMilis = 3600_000L;

    @Value("${app.sessions.tokensKeepAliveInHours}")
    private int tokensValidityPeriodHours = 6;

    @Bean
    AuthorizationContext authorizationContext(UserSessionStore sessionStore) {
        return new AuthorizationContext(sessionStore);
    }

    @Bean
    Authentication authorization(PasswordEncoder passwordEncoder,
                                 UserPasswordStore passwordStore,
                                 UserStore userStore,
                                 UserSessionStore userSessionStore,
                                 AppMailer mailer) {
        return new Authentication(
                passwordEncoder,
                passwordStore,
                userStore,
                userSessionStore,
                mailer,
                tokensValidityPeriodHours
        );
    }

    @Bean
    UserAdministration userAdministration(UserStore userStore,
                                          AppMailer mailer,
                                          Authentication authentication,
                                          UserDepartmentStore userDepartmentStore,
                                          PermissionProvider grants) {
        return new UserAdministration(userStore, mailer, authentication, userDepartmentStore, grants);
    }

    @Bean
    SystemAdministration systemAdministration(UserAdministration userAdministration,
                                              NotificationManagement notificationMgmnt,
                                              OrgDataExcelReader excelReader,
                                              PermissionProvider grants) {
        return new SystemAdministration(userAdministration, notificationMgmnt, excelReader, grants);
    }

    @Bean
    NotificationManagement notificationManagement(PermissionProvider grants,
                                                  NotificationStore notificationStore,
                                                  NotificationGrantStore grantsStore,
                                                  UserStore userStore,
                                                  UserDepartmentStore departmentStore) {
        return new NotificationManagement(grants, notificationStore, grantsStore, userStore, departmentStore);
    }


    @Bean
    @ConditionalOnMissingBean
    PermissionProvider permissionProvider() {
        return new DefaultPermissionProvider();
    }

    @Bean
    OrgDataExcelReader orgDataExcelReader() {
        return new OrgDataExcelReaderImpl();
    }

    @Bean
    UserSessionStore userSessionStore() {
        return new SessionInMemoryStoreImpl(sessionExpirationTimeMilis);
    }


    @Bean
    @ConditionalOnMissingBean
    AppMailer fakeAppMailer() {
        return new FakeMailer();
    }


    @Bean
    PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {

            @Override
            public String encode(String password) {
                return DigestUtils.md5DigestAsHex(password.getBytes());
            }

            @Override
            public boolean matches(String encodedPass, String providedPass) {
                return encodedPass.equals(DigestUtils.md5DigestAsHex(providedPass.getBytes()));
            }
        };
    }


}
