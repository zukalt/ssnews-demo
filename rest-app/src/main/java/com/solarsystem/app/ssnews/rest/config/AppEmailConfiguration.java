package com.solarsystem.app.ssnews.rest.config;

import com.solarsystem.app.ssnews.rest.impl.AppMailerImpl;
import com.solarsystem.app.ssnews.uc.ports.AppMailer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSender;
import org.thymeleaf.TemplateEngine;

@Configuration
public class AppEmailConfiguration {

    @Value("${app.web.url}")
    private String appURL;


    @Bean
    @Profile("prod")
    AppMailer mailer(JavaMailSender mailSender, TemplateEngine templateEngine) {
        return new AppMailerImpl(mailSender, templateEngine, appURL);
    }

}
