package com.solarsystem.app.ssnews.rest.config;


import com.solarsystem.app.ssnews.model.Department;
import com.solarsystem.app.ssnews.model.User;
import com.solarsystem.app.ssnews.model.UserCredential;
import com.solarsystem.app.ssnews.uc.ports.PasswordEncoder;
import com.solarsystem.app.ssnews.uc.ports.rep.UserDepartmentStore;
import com.solarsystem.app.ssnews.uc.ports.rep.UserPasswordStore;
import com.solarsystem.app.ssnews.uc.ports.rep.UserStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.UUID;

import static com.solarsystem.app.ssnews.model.Department.SYSTEM_DEPARTMENT;


@Configuration
public class AppInitialization {

    @Value("${app.admin.email}")
    private String administratorEmail;

    @Autowired
    private UserStore userStore;
    @Autowired
    private UserPasswordStore passwordStore;

    @Autowired
    private PasswordEncoder pencoder;

    @Bean
    CommandLineRunner commandLineRunner(UserDepartmentStore departments, UserStore users) {
        return (args) -> {

            if (!departments.findById(SYSTEM_DEPARTMENT.getId()).isPresent()) {
                departments.save(SYSTEM_DEPARTMENT);
            }

            if (!users.findByEmail(administratorEmail).isPresent()) {
                users.save(new User(UUID.randomUUID().toString(), administratorEmail, "System Admin", SYSTEM_DEPARTMENT));
            }
        };
    }

    @Bean
    @Profile("dev")
    CommandLineRunner commandLineRunnerDev() {
        return args -> {
            injectUser("admin", "Admin", SYSTEM_DEPARTMENT, "admin");
        };
    }

    private void injectUser(String email, String fullName, Department department, String password) {
        if (userStore.findByEmail(email).isPresent()) {
            return;
        }
        userStore.save(new User(UUID.randomUUID().toString(), email, fullName, department));
        UserCredential uc = new UserCredential(email);
        uc.setPasswordHash(pencoder.encode(password));
        System.err.println("Generated password for " + email + " is " + password);
        passwordStore.save(uc);
    }

}
