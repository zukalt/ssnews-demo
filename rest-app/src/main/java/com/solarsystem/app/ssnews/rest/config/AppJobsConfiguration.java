package com.solarsystem.app.ssnews.rest.config;

import com.solarsystem.app.ssnews.uc.ports.rep.UserSessionStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class AppJobsConfiguration {

    @Autowired
    UserSessionStore sessionStore;

    @Scheduled(fixedRateString = "${app.sessions.keepAliveTimeMs}")
    @Async
    public void pruneTimedOutSessions() {
        sessionStore.cleanExpiredSessions();
    }
}
