package com.solarsystem.app.ssnews.rest.impl;


import com.solarsystem.app.ssnews.model.User;
import com.solarsystem.app.ssnews.model.UserCredential;
import com.solarsystem.app.ssnews.uc.ex.UnsuccessfulOperationException;
import com.solarsystem.app.ssnews.uc.ports.AppMailer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Locale;


public class AppMailerImpl implements AppMailer {

    private JavaMailSender mailSender;
    private TemplateEngine templateEngine;
    private String appUrlBase;

    public AppMailerImpl(JavaMailSender mailSender, TemplateEngine templateEngine, String appUrlBase) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
        this.appUrlBase = appUrlBase;
    }

    @Async
    @Override
    public void sendUserActivationEmail(User user, UserCredential activationToken) throws UnsuccessfulOperationException {
        sendEmail(user, activationToken, "mail/activation.html", "Welcome to SolarSystem News App");
    }
    @Async
    @Override
    public void sendPasswordResetEmail(User user, UserCredential uc) throws UnsuccessfulOperationException {
        sendEmail(user, uc, "mail/reset-password.html", "Password reset was requested");
    }

    private void sendEmail(User user, UserCredential activationToken, String template, String subject) throws UnsuccessfulOperationException {
        final Context ctx = new Context(Locale.ENGLISH);
        ctx.setVariable("user", user);
        ctx.setVariable("credentials", activationToken);
        ctx.setVariable("URL", appUrlBase);
        ctx.setVariable("logo", "logo");

        final String htmlContent = this.templateEngine.process(template, ctx);
        MimeMessage message = mailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom("do-not-reply@solarsystem.com", "SolarSystem News App Activation");
            helper.setTo(activationToken.getEmail());
            helper.setSubject(subject);
            helper.setText(htmlContent, true);
            helper.addInline("logo", new ClassPathResource("templates/mail/logo.png"));

            mailSender.send(message);
        }
        catch (MessagingException | UnsupportedEncodingException e) {
            throw new UnsuccessfulOperationException(e.getMessage());
        }
    }

}
