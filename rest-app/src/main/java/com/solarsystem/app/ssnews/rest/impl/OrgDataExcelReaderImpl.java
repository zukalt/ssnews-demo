package com.solarsystem.app.ssnews.rest.impl;

import com.solarsystem.app.ssnews.uc.dto.DepartmentDto;
import com.solarsystem.app.ssnews.uc.dto.NotificationGrantDto;
import com.solarsystem.app.ssnews.uc.dto.UserDto;
import com.solarsystem.app.ssnews.uc.ports.OrgDataExcelReader;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Consumer;

public class OrgDataExcelReaderImpl implements OrgDataExcelReader {

    public ReadResult parse(InputStream excelInput) throws IOException {
        Workbook workbook = new XSSFWorkbook(excelInput);
        Sheet usersSheet = workbook.getSheet("Users");
        Sheet depSheet = workbook.getSheet("Departments");
        Sheet grantsSheet = workbook.getSheet("NotificationGrants");

        List<String> errors = new ArrayList<>();

        Map<String, DepartmentDto> departments = readDepartments(depSheet);
        Collection<UserDto> users = readUsers(departments, usersSheet, errors);
        Collection<NotificationGrantDto> notificationGrants = readGrants(departments, grantsSheet, errors);
        return new ReadResult(departments.values(), users, notificationGrants, errors);
    }

    private Collection<NotificationGrantDto> readGrants(Map<String, DepartmentDto> departments,
                                                        Sheet grantsSheet,
                                                        List<String> errors) {
        Collection<NotificationGrantDto> grants = new ArrayList<>();
        iterateFromSecondRow(grantsSheet, row -> {
            String fromDep = row.getCell(0).getStringCellValue();
            String toDep = row.getCell(1).getStringCellValue();
            String canSendPersonal = row.getCell(2).getStringCellValue();
            if (!fromDep.isEmpty() && !toDep.isEmpty()) {
                grants.add(new NotificationGrantDto(null, fromDep, toDep, "yes".equalsIgnoreCase(canSendPersonal)));
                if (!departments.containsKey(fromDep)){
                    addDepartmentError(errors, fromDep);
                }
                if (!departments.containsKey(toDep)){
                    addDepartmentError(errors, toDep);
                }
            }
        });
        return grants;
    }

    public Collection<UserDto> readUsers(Map<String, DepartmentDto> departments,
                                         Sheet usersSheet,
                                         List<String> errors) {
        Collection<UserDto> users = new ArrayList<>();

        iterateFromSecondRow(usersSheet, row -> {
            UserDto user = new UserDto();
            user.email = row.getCell(0).getStringCellValue();
            user.fullName = row.getCell(1).getStringCellValue();
            Date d = row.getCell(2).getDateCellValue();
            user.birthDate = LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(d));

            String depId = row.getCell(3).getStringCellValue();
            if (departments.containsKey(depId)) {
                user.department = departments.get(depId);
            }
            else {
                addDepartmentError(errors, depId);
                user.department = new DepartmentDto(depId, null);
            }

            if (!user.email.isEmpty() && !depId.isEmpty()) {
                users.add(user);
            }
        });

        return users;
    }

    Map<String, DepartmentDto> readDepartments(Sheet departments) {
        final Map<String, DepartmentDto> deps = new HashMap<>();

        iterateFromSecondRow(departments, row -> {
            String id = row.getCell(0).getStringCellValue();
            String name = row.getCell(1).getStringCellValue();
            if (!id.isEmpty() && !name.isEmpty()) {
                deps.put(id, new DepartmentDto(id, name));
            }
        });

        return deps;
    }

    private void iterateFromSecondRow(Sheet sheet, Consumer<Row> consumer) {
        Iterator<Row> rowIterator = sheet.rowIterator();
        rowIterator.next(); // skip header
        rowIterator.forEachRemaining(consumer);
    }

    private void addDepartmentError(List<String> errors, String missingDepartment){
        errors.add("Department.id = "+missingDepartment+" is missing in Departments sheet");
    }
}
