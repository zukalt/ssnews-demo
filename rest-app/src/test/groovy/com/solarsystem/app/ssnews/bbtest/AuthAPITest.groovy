package com.solarsystem.app.ssnews.bbtest


import org.junit.Test
import org.springframework.beans.factory.annotation.Value


class AuthAPITest extends SpringBootTestBase {

    @Value('${app.admin.email}')
    String adminEmail

    @Test
    void adminToRestorePasswordThenLoginAndLogout() {

        doPost("/auth/forgot-password", [
                        email: adminEmail
        ], null).expect(201)

        doPost("/auth/create-password", [
                        email      : adminEmail,
                        newPassword: adminEmail.reverse(),
                        token      : fakeMailer.lastMailSentUserCredential.passwordResetToken
        ], null).expect(201)


        String token = login(adminEmail, adminEmail.reverse()).expect(200).token

        doGet("/auth/session", null).expect (401) // auth headers not supplied

        def data = doGet("/auth/session", token).expect(200)
        assert data.user.email == adminEmail
        assert data.user.department != null
        assert data.user.department.id == "SYSTEM"

        doGet("/auth/logout", null).expect(200) // auth headers not supplied
        doGet("/auth/logout", token).expect(200)
        // already logged out
        doGet("/auth/logout", token).expect(200)

        // after logout token should be expired
        doGet("/auth/session", token).expect(401)
        doGet("/auth/session", null).expect(401) // auth headers not supplied
    }

    @Test
    void changePasswordFlow() {
        def session = resetPasswordAndLogin([email: adminEmail, password: adminEmail.reverse()])
        String token = session.token

        doPost("/auth/change-password", [
                oldPassword: 'wrong-old-pass',
                newPassword: 'new-pass'
        ], token).expect(401) // incorrect old pass


        doPost("/auth/change-password", [
                oldPassword: adminEmail.reverse(),
                newPassword: 'new-pass'
        ], 'invalid-session-token').expect(401) // unauthorized


        doPost("/auth/change-password", [
                oldPassword: adminEmail.reverse(),
                newPassword: 'new-pass'
        ], token).expect(200) // OK, changed

        // check if changed
        login(adminEmail, adminEmail.reverse()).expect(401) // old pass not valid
        login(adminEmail, 'new-pass').expect(200) // successful
    }

    @Test
    void forgotPasswordOnNonExistingUserShouldKeepSilence() {
        doPost("/auth/forgot-password", [
                email: "email-of-a-not-registered-user@gmail.com"
        ], null).expect(201)
    }


}
