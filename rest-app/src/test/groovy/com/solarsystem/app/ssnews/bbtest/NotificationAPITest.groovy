package com.solarsystem.app.ssnews.bbtest

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters
import org.springframework.beans.factory.annotation.Value
import org.springframework.test.annotation.DirtiesContext

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class NotificationAPITest extends SpringBootTestBase {

    @Value('${app.admin.email}')
    String adminEmail

    String adminSessionToken, adminUserId
    def departments, users
    def devUser, hrUser, managerUser

    @Before
    void populateDatabase() {
        if (adminSessionToken == null) {
            def admin = resetPasswordAndLogin([email: adminEmail, password: adminEmail.reverse()])
            adminSessionToken = admin.token;
            adminUserId = admin.user.id;
            initDatabase(adminSessionToken)
            departments = doGet('/departments/', adminSessionToken).expect(200)
            assert departments.size() == 6
            users = doGet('/users/', adminSessionToken).expect(200)
            devUser = users.find { it.department.id == "DEV" }
            hrUser = users.find { it.department.id == "HR" }
            managerUser = users.find { it.department.id == "MANAGEMENT" }
            (devUser, hrUser, managerUser) = [devUser, hrUser, managerUser].collect({
                resetPasswordAndLogin([email: it.email, password: it.id])
            })
        }
    }

    @Test
    void test02_basicTestAllFlow() {
        Map<String, Map<String, Long>> stats = getStats()
        assert stats.sentPerDepartment.isEmpty()
        assert stats.receivedPerDepartment.isEmpty()

        assert grants(adminSessionToken).isEmpty() // SYSTEM pseudo department has no grants

        assert grants(hrUser.token).entrySet().size() == 5 // HR can send to departments
        assert grants(hrUser.token).values().every({ it }) // HR can send PERSONAL to departments

        assert grants(devUser.token).keySet().containsAll(["DEV", "MANAGEMENT"]) // Dev can send to departments

        doPost("/notifications/send-personal",
                [
                        "body"   : "Happy birthday ${devUser.user.fullName}, You can leave earlier today. :) ",
                        "subject": "Happy Birthday",
                        "userId" : devUser.user.id
                ],
                hrUser.token
        ).expect(200)

        stats = getStats()
        assert stats.sentPerDepartment.size() == 1
        assert stats.sentPerDepartment["HR"] == 1
        assert stats.receivedPerDepartment.size() == 1
        assert stats.receivedPerDepartment["DEV"] == 1

        def allDepartmentsButSystem = departments.findAll { d -> d.id != "SYSTEM" }.collect {d -> d.id}
        doPost("/notifications/send",
                [
                        "subject"      : "You are all invited",
                        "body"         : "To have some bear at our room",
                        "toDepartments": allDepartmentsButSystem
                ],
                devUser.token
        ).expect(401) // forbidden, only to "DEV" and "MANAGEMENT"
        stats = getStats() // Make sure didn't actually send
        assert stats.sentPerDepartment.size() == 1
        assert stats.sentPerDepartment["HR"] == 1
        assert stats.receivedPerDepartment.size() == 1
        assert stats.receivedPerDepartment["DEV"] == 1

        doPost("/notifications/send",
                [
                        "subject"      : "You are all invited",
                        "body"         : "To have some bear at our room",
                        "toDepartments": ["DEV", "MANAGEMENT"]
                ],
                devUser.token
        ).expect(200) // ok dor "DEV" and "MANAGEMENT"

        // Developer can't send personal
        doPost("/notifications/send-personal",
                [
                        "body"   : "Happy birthday ${hrUser.user.fullName}",
                        "subject": "Happy Birthday from Development team",
                        "userId" : hrUser.user.id
                ],
                devUser.token
        ).expect(401)

        def totalDeveloper = users.findAll { u -> u.department.id == "DEV"} .size()
        def totalManagers = users.findAll { u -> u.department.id == "MANAGEMENT"} .size()
        stats = getStats() // Make sure didn't actually send
        assert stats.sentPerDepartment.size() == 2
        assert stats.sentPerDepartment["HR"] == 1
        assert stats.sentPerDepartment["DEV"] == 1
        assert stats.receivedPerDepartment.size() == 2
        assert stats.receivedPerDepartment["DEV"] == 1 + totalDeveloper
        assert stats.receivedPerDepartment["MANAGEMENT"] == totalManagers

        def devNotifications = doGet("/notifications/", devUser.token).expect(200)
        assert devNotifications.size() == 2 // one sent from hr and their own
        assert devNotifications[1].notification.createdBy.department.id == "HR"
        assert devNotifications[1].notification.createdBy.email == hrUser.user.email

        assert devNotifications[0].notification.createdBy.department.id == "DEV"
        assert devNotifications[0].notification.createdBy.email == devUser.user.email

        doDelete("/notifications/${devNotifications[0].id}", devUser.token)
        devNotifications = doGet("/notifications/", devUser.token).expect(200)
        assert devNotifications.size() == 1 // one sent from hr and their own
        assert devNotifications[0].notification.createdBy.department.id == "HR"
        assert devNotifications[0].read == false
        // mark read
        doPut("/notifications/${devNotifications[0].id}", "true", devUser.token)
        devNotifications = doGet("/notifications/", devUser.token).expect(200)
        assert devNotifications[0].read == true

    }

    @Test
    void test03_grantAndIgnoreListsTest(){
        // update HR grants, disable personal
        doPost("/notifications/grants", [
                "fromDepartment"              : "HR",
                "personalNotificationsAllowed": false,
                "toDepartment"                : "DEV"
        ], adminSessionToken)
                .expect(201)

        doPost("/notifications/send-personal",
                [
                        "body"   : "Happy birthday ${devUser.user.fullName}, You can leave earlier today. :) ",
                        "subject": "Happy Birthday",
                        "userId" : devUser.user.id
                ],
                hrUser.token
        ).expect(401)

        doPut("/users/my/ignoreList", ["HR"], devUser.token).expect(200)
        // make sure it is saved
        assert doGet("/users/my/ignoreList", devUser.token).expect(200). find {it == "HR"} != null

        // still HR can send to DEV group
        doPost("/notifications/send",
                [
                        "subject"      : "Important notification",
                        "body"         : "Don't put HR to ignore list",
                        "toDepartments": ["DEV"]
                ],
                hrUser.token
        ).expect(200)


        def devNotifications = doGet("/notifications/", devUser.token).expect(200)

        assert devNotifications.find { it.notification.subject == "Important notification"} == null

        doPut("/users/my/ignoreList", [], devUser.token).expect(200)
        // make sure it is saved
        assert doGet("/users/my/ignoreList", devUser.token).expect(200).size() == 0

    }

    Map<String, Boolean> grants(token) {
        return doGet('/notifications/grants/my', token).expect(200) as Map<String, Boolean>
    }

    Map<String, Map<String, Long>> getStats() {
        return doGet('/notifications/stats', adminSessionToken).expect(200) as Map<String, Map<String, Long>>
    }
}
