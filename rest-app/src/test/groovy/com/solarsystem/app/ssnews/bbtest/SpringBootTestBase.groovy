package com.solarsystem.app.ssnews.bbtest

import com.solarsystem.app.ssnews.rest.Application
import com.solarsystem.app.ssnews.uc.impl.FakeMailer
import groovyx.net.http.*
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

import java.time.LocalDate

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [Application.class])
@ActiveProfiles("test")
abstract class SpringBootTestBase {


    @LocalServerPort
    int randomServerPort

    String contextPath = '/api'

    @Value('${app.admin.email}')
    String adminEmail

    @Autowired
    FakeMailer fakeMailer

    HttpBuilder restClient

    @BeforeClass
    static void init() {

    }

    class ApiResponse {
        Object data
        int status

        ApiResponse(int status, Object body) {
            this.status = status
            data = body
        }

        def expect(int status) {
            assert this.status == status
            return data
        }

        def getAt(int idx) {
            if (idx == 0) data
            else if (idx == 1) status
            else throw new Exception("Wrong index, use 0 or 1")
        }
    }

    @Before
    void setup() {
        if (restClient == null) {
            restClient = HttpBuilder.configure {
                request.uri = "http://localhost:" + randomServerPort
                request.contentType = 'application/json'
                request.accept = 'application/json'
                request.charset = 'UTF-8'

                response.success { FromServer fs, Object body ->
                    return new ApiResponse(fs.statusCode, body)
                }
                response.failure { FromServer fs, Object body ->
                    return new ApiResponse(fs.statusCode, body)
                }
            }
        }
    }

    @After
    void clean() {
        fakeMailer.clean()
    }

    ApiResponse doGet(String path, String authToken = null) {
        return restClient.get {
            request.uri.path = contextPath + path
            request.headers['Authorization'] = authToken
        } as ApiResponse
    }


    ApiResponse doDelete(String path, String authToken) {
        return restClient.delete {
            request.uri.path = contextPath + path
            request.headers['Authorization'] = authToken
        } as ApiResponse
    }

    ApiResponse doPost(String path, Object body, String authToken = null) {
        return restClient.post {
            request.uri.path = contextPath + path
            if (authToken) {
                request.headers['Authorization'] = authToken
            }
            request.body = body
        } as ApiResponse
    }


    ApiResponse doPut(String path, Object body, String sessionToken) {
        return restClient.put {
            request.uri.path = contextPath + path
            request.headers['Authorization'] = sessionToken
            request.body = body
        } as ApiResponse
    }

    ApiResponse initDatabase(String sessionToken) {
        def filePath = getClass().getClassLoader().getResource('sample-data-import.xlsx').file
        File file = new File(filePath)
        return restClient.post {
            request.uri.path = contextPath + '/system/initDatabase'
            request.contentType = 'multipart/form-data'
            request.headers['Authorization'] = sessionToken

            request.body = MultipartContent.multipart {
                part 'file', file.name, 'application/octet-stream', file
            }
            request.encoder 'multipart/form-data', ApacheEncoders.&multipart
        } as ApiResponse
    }

    def resetPasswordAndLogin(user) {
        doPost("/auth/forgot-password", [email: user.email])
                .expect(201)

        doPost("/auth/create-password", [
                email      : user.email,
                newPassword: user.password,
                token      : fakeMailer.lastMailSentUserCredential.passwordResetToken
        ])
                .expect(201)

        return login(user.email, user.password).expect(200)
    }

    ApiResponse login(String email, String password) {
        return restClient.post {
            request.contentType = ContentTypes.URLENC[0]
            request.uri.path = contextPath + '/auth/login'
            request.body = [
                    email   : email,
                    password: password
            ]
        } as ApiResponse
    }

    ApiResponse logout(String sessionToken) {
        return doGet("/auth/logout", sessionToken)
    }


}
