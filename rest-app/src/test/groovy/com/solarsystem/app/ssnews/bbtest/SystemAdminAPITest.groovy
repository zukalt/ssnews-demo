package com.solarsystem.app.ssnews.bbtest


import org.junit.Test
import org.springframework.beans.factory.annotation.Value



class SystemAdminAPITest extends SpringBootTestBase {

    @Value('${app.admin.email}')
    String adminEmail


    @Test
    void testImportAppData() {
        def session = resetPasswordAndLogin([email: adminEmail, password: adminEmail.reverse()])

        initDatabase(session.token).expect(200)

        def users = doGet('/users/', session.token).expect(200)
        assert users.size() == 40

        def departments = doGet('/departments/', session.token).expect(200)
        assert departments.size() == 6

        def grants = doGet('/notifications/grants', session.token).expect(200)
        assert grants.size() == 18

        // subsequent import should be rejected
        initDatabase(session.token).expect(409) // conflict, db already initialized
    }


}
