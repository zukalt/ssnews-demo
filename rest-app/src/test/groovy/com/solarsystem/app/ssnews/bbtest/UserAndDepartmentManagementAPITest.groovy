package com.solarsystem.app.ssnews.bbtest

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters
import org.springframework.beans.factory.annotation.Value
import org.springframework.test.annotation.DirtiesContext

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class UserAndDepartmentManagementAPITest extends SpringBootTestBase {

    @Value('${app.admin.email}')
    String adminEmail


    @Test
    void case01_userCRU() {
        def session = resetPasswordAndLogin([email: adminEmail, password: adminEmail.reverse()])

        def allUsers = loadAllUsers(session.token);


        assertArraysSameByField(searchUser(session.token, "%").result as Object[], allUsers, { it.id })

        def newUserData = [
                email     : "new-user-email@company.com",
                fullName  : "Jane Doe",
                department: "SYSTEM",
                birthDate : "1990-01-01"
        ]

        def created = createUser(session.token, newUserData);
        // find in search all request
        def searchResult = searchUser(session.token, "%")
        assert searchResult.total == allUsers.size() + 1
        assert searchResult.result.find { it.email == newUserData.email }
        // find in all users
        assert loadAllUsers(session.token).find { it.email == newUserData.email } != null

        updateUser(session.token, created.id, "John", "SYSTEM");

        assert searchUser(session.token, "John").result.find { it.id == created.id } != null
        assert searchUser(session.token, "Jo%").result.find { it.id == created.id } != null
        assert searchUser(session.token, "%ohn").result.find { it.id == created.id } != null


        createUser(session.token, newUserData, 409) // unique email violation

    }

    @Test
    void case02_departmentsCRU() {
        def session = resetPasswordAndLogin([email: adminEmail, password: adminEmail.reverse()])
        doPost("/departments/", [
                id: "SYSTEM", name: "System department"
        ], session.token).expect(409) // already exists

        doPut("/departments/SYSTEM", [
                id: "SYSTEM", name: "System Administrators"
        ], session.token).expect(200) // already exists

        def updated = doGet("/departments/", session.token).expect(200).find {it.id == "SYSTEM"}
        assert updated.name == "System Administrators"

        def allDeps = doGet("/departments/", session.token).expect(200)
        assert allDeps.find { it.id == "SYSTEM" }.name == updated.name

        //
        // Creating new
        //
        doPost("/departments/", [
                id: "SYSTEM2", name: "System Department"
        ], session.token).expect(201)
        updated = doGet("/departments/", session.token).expect(200).find {it.id == "SYSTEM2"}
        assert updated.id == "SYSTEM2"
        assert updated.name == "System Department"

        def newUserData = [
                email     : "admin2l@company.com",
                fullName  : "Admin2",
                department: "SYSTEM2",
                birthDate : "1990-01-01"
        ]

        def created = createUser(session.token, newUserData);

        doPost("/notifications/grants", [
                "fromDepartment"              : "SYSTEM",
                "personalNotificationsAllowed": false,
                "toDepartment"                : "SYSTEM2"
        ], session.token)
                .expect(201)

        // can't send personal
        doPost("/notifications/send-personal", [
                "body"   : "test",
                "subject": "test",
                "userId" : created.id
        ], session.token)
                .expect(401)

        // but can to department
        doPost("/notifications/send", [
                "body"         : "test",
                "subject"      : "test",
                "toDepartments": ["SYSTEM2"]
        ], session.token)
                .expect(200)

        def allGrants = doGet("/notifications/grants", session.token).expect(200);
        def grant = allGrants.find {it.fromDepartment == "SYSTEM" && it.toDepartment == "SYSTEM2"}
        // revoke grant and test send is blocked
        doDelete("/notifications/grants/${grant.id}", session.token)
                .expect(200)
        doPost("/notifications/send", [
                "body"         : "test",
                "subject"      : "test",
                "toDepartments": ["SYSTEM2"]
        ], session.token)
                .expect(401)

    }


    void updateUser(String sessionToken, String userId, String fullName, String department) {
        doPut("/users/${userId}", [
                fullName: fullName, department: department
        ], sessionToken).expect(200)
    }

    boolean assertArraysSameByField(Object[] a1, Object[] a2, Closure field = { it.id }) {
        assert a1.length == a2.length
        a1 = a1.collect(field).sort()
        a2 = a2.collect(field).sort()
        a1.eachWithIndex { Object entry, int i -> assert entry == a2[i] }
    }

    Object createUser(String sessionToken, Object createUserData, expectStatus = 201) {
        return doPost("/users/", createUserData, sessionToken).expect(expectStatus)
    }

    Object searchUser(String sessionToken, String nameLike, page = 0, pageSize = 1000) {
        return doPost("/users/search", [
                nameLike: nameLike, page: page, pageSize: pageSize
        ], sessionToken).expect(200)
    }

    Object[] loadAllUsers(String sessionToken) {
        return doGet("/users/", sessionToken).expect(200)
    }

}
