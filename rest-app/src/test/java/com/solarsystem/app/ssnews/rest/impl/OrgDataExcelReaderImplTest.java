package com.solarsystem.app.ssnews.rest.impl;

import com.solarsystem.app.ssnews.uc.ports.OrgDataExcelReader;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

public class OrgDataExcelReaderImplTest {


    @Test
    public void basicParse() throws IOException {
        InputStream excelFile = getClass().getClassLoader().getResourceAsStream("sample-data-import.xlsx");
        OrgDataExcelReader reader = new OrgDataExcelReaderImpl();

        AtomicInteger count = new AtomicInteger(0);
        LocalDate birthDatesStart = LocalDate.of(2000, 4, 1);
        OrgDataExcelReader.ReadResult result = reader.parse(excelFile);

        result.departmentList.forEach(dep -> {
            count.incrementAndGet();
            assertNotNull(dep);
            assertNotNull(dep.id);
            assertNotNull(dep.name);
        });
        assertEquals(5, count.get());

        count.set(0);
        result.userList.forEach(userDto -> {
            assertNotNull(userDto);
            assertNotNull(userDto.email);
            assertNotNull(userDto.fullName);
            assertNotNull(userDto.department);
            assertNotNull(userDto.department.id);
            assertNotNull(userDto.department.name);
            assertNotNull(userDto.birthDate);
            assertEquals(birthDatesStart.plusDays(count.get()), userDto.birthDate);
            count.incrementAndGet();
        });

        assertEquals(39, count.get());

        count.set(0);
        result.notificationGrants.forEach(grantDto -> {
            assertNotNull(grantDto);
            assertNull(grantDto.id);
            assertNotNull(grantDto.fromDepartment);
            assertNotNull(grantDto.toDepartment);
            count.incrementAndGet();
        });

        assertEquals(18, count.get());
    }
}
