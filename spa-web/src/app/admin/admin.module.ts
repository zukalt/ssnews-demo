import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SearchComponent} from './search/search.component';
import {ViewUserComponent} from './view-user/view-user.component';
import {EditUserComponent} from './edit-user/edit-user.component';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from '../shared/pages';
import {CommonDepsModule} from '../shared/common-deps.module';
import {DialogsModule} from '../shared/dialogs.module';
import {ConfirmationComponent} from '../shared/dialogs';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AdminGuard, SessionInterceptor} from '../shared/services';


const routes: Routes = [
  {
    path: '', pathMatch: 'full',
    component: SearchComponent, canActivate: [AdminGuard]
  },
  {
    path: 'view/:id',
    component: ViewUserComponent
  },
  {
    path: 'edit/:id',
    component: EditUserComponent
  },
  {
    path: '**', component: PageNotFoundComponent
  }

];


@NgModule({
  declarations: [SearchComponent, ViewUserComponent, EditUserComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CommonDepsModule,
    DialogsModule
  ],
  exports: [
    RouterModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: SessionInterceptor, multi: true }
  ],
  entryComponents: [
    ConfirmationComponent
  ]
})
export class AdminModule { }
