import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService, SessionService, UsersService} from '../../shared/services';
import {ActivatedRoute, Router} from '@angular/router';
import {Department, User} from '../../shared/models';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  form: FormGroup;
  user: User;
  departments$: Observable<Department>;

  constructor(public session: SessionService,
              private usersService: UsersService,
              private route: ActivatedRoute,
              private router: Router,
              private notifs: NotificationService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      fullName: ['', Validators.required],
      department: ['', Validators.required],
      birthDate: [''],
      email: ['', [Validators.required, Validators.email]],
    });
    this.departments$ = this.usersService.allDepartments();
    this.usersService.loadUser(this.route.snapshot.params.id)
      .toPromise()
      .then((user) => {
        this.user = user;
        this.form.reset({
          fullName: user.fullName,
          email: user.email,
          birthDate: user.birthDate,
          department: user.department.id
        });
      });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    this.user.fullName = this.form.controls.fullName.value;
    this.user.department = this.form.controls.department.value;

    this.usersService.updateUser(this.user).toPromise().then(
      () => this.notifs.success('Saved')
    ).catch(err => this.notifs.error('Update filed: ' + err));
  }

  back() {
    this.router.navigate(['/admin/view', this.route.snapshot.params.id]).then();
  }
}
