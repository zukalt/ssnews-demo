import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {SearchUserResult, User} from '../../shared/models';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {UsersService} from '../../shared/services';
import {debounceTime, distinctUntilChanged, map, startWith, tap} from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['fullName', 'email', 'roles'];
  usersDS: UsersDataSource;

  @ViewChild(MatPaginator, {static: true})
  paginator: MatPaginator;
  searchTerm: string;

  constructor(private usersService: UsersService) {
    this.usersDS = new UsersDataSource(usersService);
  }


  ngOnInit() {
    this.searchTerm = '';
  }

  ngAfterViewInit() {
    const pageSize = this.paginator.pageSize;
    const paging$ = this.paginator.page.pipe(
      startWith({pageIndex: 0, pageSize} as PageEvent)
    );

    this.usersDS.attachPaginator(paging$, this.paginator);
    this.applyFilter('');
  }

  applyFilter(value: string) {
    this.usersDS.setFilterText(value);
  }

  reload() {
    this.applyFilter('');
  }

  get total$() {
    return this.usersDS.totalFound$;
  }
}

class UsersDataSource implements DataSource<User> {

  private usersSubject = new BehaviorSubject<SearchUserResult>({result: [], total: 0});
  private filterSubject = new BehaviorSubject('%');

  constructor(private usersService: UsersService) {
  }

  attachPaginator(paging$: Observable<PageEvent>, paginator: MatPaginator) {

    const debouncedSearches$ = this.filterSubject.pipe(
                                                    debounceTime(300),
                                                    distinctUntilChanged(),
                                                    tap(() => paginator.firstPage()) // if filter changed, set to pageStart
                                                  );

    combineLatest(paging$, debouncedSearches$)
      .pipe(
        tap(([page, text]) => {
          this.loadData(text, page.pageIndex, page.pageSize);
        })
      ).subscribe();

  }

  connect(collectionViewer: CollectionViewer): Observable<User[]> {
    return this.usersSubject.pipe(map((sr) => sr.result));
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.usersSubject.complete();
    this.filterSubject.complete();
  }

  setFilterText(text: string) {
    this.filterSubject.next(`%${text}%`);
  }

  get totalFound$() {
    return this.usersSubject.pipe(map(s => s.total));
  }

  loadData(text: string, pageIndex: number, pageSize: number) {
    this.usersService.search(text, pageIndex, pageSize).subscribe(
      sr => this.usersSubject.next(sr)
    );
  }
}
