import {Component, OnInit} from '@angular/core';
import {SessionService, UsersService} from '../../shared/services';
import {Observable} from 'rxjs';
import {User} from '../../shared/models';
import {ActivatedRoute, Router} from '@angular/router';
import {map, mergeMap} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss']
})
export class ViewUserComponent implements OnInit {

  user$: Observable<User>;
  private userId: string;

  constructor(public session: SessionService,
              private usersService: UsersService,
              private route: ActivatedRoute,
              private router: Router,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.reload();
  }

  editUser() {
    this.router.navigate(['/admin/edit', this.route.snapshot.params.id]);
  }


  private reload() {
    this.user$ = this.route.params.pipe(
      map(params => this.userId = params.id),
      mergeMap( userId => this.usersService.loadUser(userId))
    );
  }
}
