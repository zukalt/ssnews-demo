import {Component} from '@angular/core';
import {AuthenticationService, SessionService} from './shared/services';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private authService: AuthenticationService,
              public session: SessionService,
              private router: Router) {
  }


  pathStartsWith(path: string) {
    return this.router.url.startsWith(path);
  }
  pathIs(path: string) {
    return this.router.url === path;
  }
}
