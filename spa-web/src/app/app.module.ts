import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {RouterModule, Routes} from '@angular/router';

import {AppComponent} from './app.component';
// Batch Import main dependencies from CommonModule
import {CommonDepsModule} from './shared/common-deps.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS} from '@angular/material/snack-bar';
import {
    AdminGuard,
    ApiErrorInterceptor,
    AuthenticationService,
    AuthGuard, NotificationsDataService,
    NotificationService,
    SessionInterceptor,
    SessionService,
    SnackbarMessageComponent
} from './shared/services';
import {PageNotFoundComponent, ProfileComponent} from './shared/pages/';
import {AuthModule} from './auth/auth.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DashboardModule} from './dashboard/dashboard.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {NotificationStatsComponent} from './shared/component';
import {AdminService} from './shared/services/admin.service';

const routes: Routes = [
    // { path: 'auth', ... } see eagerly loaded in AuthModule
    {
        path: '', component: NotificationStatsComponent, canActivate: [AuthGuard]
    },
    {
        path: 'notifications', component: DashboardComponent, canActivate: [AuthGuard]
    },
    {
        path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]
    },
    {
        path: 'admin',
        loadChildren: () => import('./admin/admin.module').then(mod => mod.AdminModule),
        canLoad: [AdminGuard],
        canActivateChild: [AdminGuard],
        canActivate: [AdminGuard],
    },
    {
        path: '**', component: PageNotFoundComponent
    }
];


@NgModule({
    declarations: [
        AppComponent,
        ProfileComponent,
        SnackbarMessageComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(routes, {enableTracing: false}),
        CommonDepsModule,
        HttpClientModule,
        AuthModule, // eager loading
        DashboardModule
    ],
    exports: [],
    providers: [
        AuthGuard, AdminGuard,
        SessionService,
        NotificationService,
        AuthenticationService,
        NotificationsDataService,
        AdminService,

        {provide: HTTP_INTERCEPTORS, useClass: SessionInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ApiErrorInterceptor, multi: true},
        {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 3000, verticalPosition: 'top'}}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
