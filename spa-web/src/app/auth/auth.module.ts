import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CommonDepsModule} from '../shared/common-deps.module';

import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ForgotPassComponent} from './forgot-pass/forgot-pass.component';
import {ResetPassComponent} from './reset-pass/reset-pass.component';
import {SnackbarMessageComponent, UsersService} from '../shared/services';

export const authRoutes: Routes = [
  {
    path: 'auth',
    children: [
      {
        path: '', pathMatch: 'full', component: LoginComponent
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'logout',
        component: LoginComponent,
        data: {logout: true},
      },
      {
        path: 'register',
        component: RegisterComponent,
      },
      {
        path: 'create-password',
        component: ResetPassComponent,
      },
      {
        path: 'forgot-password',
        component: ForgotPassComponent,
      }]
  },
];

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    ForgotPassComponent,
    ResetPassComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(authRoutes),
    CommonDepsModule
  ],
  exports: [
  ],
  providers: [
    UsersService
  ],
  entryComponents: [
    SnackbarMessageComponent
  ]
})
export class AuthModule {
}
