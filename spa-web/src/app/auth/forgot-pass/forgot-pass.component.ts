import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService, NotificationService} from '../../shared/services';

@Component({
  selector: 'app-forgot-pass',
  templateUrl: './forgot-pass.component.html',
  styleUrls: ['../auth.components.scss']
})
export class ForgotPassComponent implements OnInit {

  group: FormGroup;

  constructor(private notifications: NotificationService,
              private authService: AuthenticationService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {

    const provided = this.route.snapshot.queryParamMap.get('email') || '';

    this.group = this.formBuilder.group({
      email: [provided, [Validators.required, Validators.email]]

    });

  }

  restorePass() {
    if (this.group.invalid) {
      this.notifications.warn('Please enter valid email');
      return;
    }

    const email = this.group.controls.email.value;
    this.group.disable();
    this.authService.forgotPassword(email).toPromise()
      .then(() => {
        this.notifications.info('Succeed, now check your email!');
      })
      .catch(
        error => {
          this.notifications.error(error);
          this.group.enable();
        });

  }
}
