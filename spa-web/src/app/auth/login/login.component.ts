import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AuthenticationService, NotificationService, SessionService} from '../../shared/services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../auth.components.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  returnTo: string;

  constructor(private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private notifs: NotificationService,
              private router: Router,
              private session: SessionService,
              private authService: AuthenticationService) {

    if (this.activatedRoute.snapshot.data.logout) {
      this.authService.logout();
    }
    if (this.session.currentSession != null) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.returnTo = this.activatedRoute.snapshot.queryParams.to || '/';
  }

  private input(name) {
    return this.loginForm.controls[name].value;
  }

  login() {
    if (this.loginForm.invalid) {
      return;
    }

    this.authService.login(this.input('email'), this.input('password'))
      .pipe(first())
      .toPromise()
      .then(() => {
        this.router.navigate(['/']);
      })
      .catch(() => {
        this.notifs.error('Access denied. Invalid email/password.');
        this.loginForm.controls.password.reset();
      });

  }

}
