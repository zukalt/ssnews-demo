import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService, NotificationService} from '../../shared/services';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../auth.components.scss']
})
export class RegisterComponent implements OnInit {
  registration: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private notifications: NotificationService,
              private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.registration = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      fullName: ['', Validators.required]
    });
  }

  onSubmit() {
    if (this.registration.invalid) {
      this.notifications.warn('All fields are required and must be valid!');
      return;
    }

    const email = this.registration.controls.email.value;
    const fullName = this.registration.controls.fullName.value;

    this.registration.disable();
    this.authService.register(email, fullName).pipe(first())
      .toPromise()
      .then(() => {
        this.notifications.info('Succeed, now check your email!');
        this.router.navigate(['/auth/message'], {
          skipLocationChange: true,
          queryParams: {msg: 'Thanks for registering!'}
        });
      })
      .catch(() => {
        this.registration.enable();
        this.notifications.error('User with such email exists');
      });


  }
}
