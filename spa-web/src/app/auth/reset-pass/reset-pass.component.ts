import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService, NotificationService} from '../../shared/services';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-reset-pass',
  templateUrl: './reset-pass.component.html',
  styleUrls: ['../auth.components.scss']
})
export class ResetPassComponent implements OnInit {
  formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private notifs: NotificationService,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      password2: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  createPassword() {
    if (this.formGroup.controls.password.value !== this.formGroup.controls.password2.value) {
      this.formGroup.reset();
      this.notifs.error('Passwords does not match, please retype.');
      return;
    }
    const email = this.route.snapshot.queryParamMap.get('email');
    const token = this.route.snapshot.queryParamMap.get('token');
    const password = this.formGroup.controls.password.value;
    console.log(this.route.snapshot.paramMap);
    this.formGroup.disable();
    this.authService.createPassword(token, email, password)
      .toPromise()
      .then(() => {
        this.notifs.info('Successfully reset, logging you in...');
        this.authService.login(email, password)
          .subscribe(() => this.router.navigate(['/']));
      })
      .catch(() => {
        this.notifs.warn('It looks like token is expired, reset again');
        this.router.navigate(['/auth/forgot-password'], {queryParams: {email}});
      });
  }
}
