import {Component, OnInit} from '@angular/core';
import {NotificationsDataService, SessionService} from '../shared/services';
import {MatTableDataSource} from '@angular/material/table';
import {UserNotification} from '../shared/models';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmationComponent} from '../shared/dialogs';
import {config} from 'rxjs';
import {SendNotificationsComponent} from './send-notifications.component';
import {ShowNotificationsComponent} from './show-notifications.component';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    dataSource = new MatTableDataSource([]);
    displayedColumns: string[] = ['subject', 'author', 'department', 'sentAt'];

    constructor(
        public  session: SessionService,
        private matDialog: MatDialog,
        private notificationsDataService: NotificationsDataService
    ) {
    }

    ngOnInit() {
        this.notificationsDataService.myNotifications().subscribe(data => this.dataSource.data = data);
    }

    sendNotification() {
        this.matDialog.open(SendNotificationsComponent, {
            width: '80%'
        }).afterClosed().subscribe(
            data => {
                this.notificationsDataService.send(data).subscribe(() => this.ngOnInit());
            }
        );
    }

    showNotification(userNotification: UserNotification) {
        this.matDialog.open(ShowNotificationsComponent, {
            width: '80%',
            data: userNotification
        }).afterClosed().subscribe(action => {
            if (action === 'delete') {
                this.notificationsDataService.deleteNotification(userNotification.id).subscribe(
                    () => this.deleteLocal(userNotification.id)
                );
            } else if (action === 'read') {
                this.notificationsDataService.markStatus(userNotification.id, true).subscribe(
                    () => this.updateLocal(userNotification.id, true)
                );
            } else if (action === 'unread') {
                this.notificationsDataService.markStatus(userNotification.id, false).subscribe(
                    () => this.updateLocal(userNotification.id, false)
                );
            }
        });
    }

    deleteLocal(id: string) {
        this.dataSource.data = this.dataSource.data.filter(d => d.id !== id) ;
    }
    updateLocal(id: string, readStatus: boolean) {
        const data = [...this.dataSource.data];
        data.find(n => n.id === id).read = readStatus;
        this.dataSource.data = data;
    }
}
