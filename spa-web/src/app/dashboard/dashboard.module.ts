import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {CommonDepsModule} from '../shared/common-deps.module';
import {ConfirmationComponent} from '../shared/dialogs';
import {DialogsModule} from '../shared/dialogs.module';
import {SendNotificationsComponent} from './send-notifications.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {ShowNotificationsComponent} from './show-notifications.component';


@NgModule({
    declarations: [DashboardComponent, SendNotificationsComponent, ShowNotificationsComponent],
    imports: [
        CommonModule,
        CommonDepsModule,
        DialogsModule,
        MatDialogModule,
        MatButtonToggleModule
    ],
    entryComponents: [ConfirmationComponent, SendNotificationsComponent, ShowNotificationsComponent]
})
export class DashboardModule {
}
