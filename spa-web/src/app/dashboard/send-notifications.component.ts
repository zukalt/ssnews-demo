import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {Department, User} from '../shared/models';
import {NotificationsDataService} from '../shared/services';


@Component({
    template: `
        <h1 mat-dialog-title>Send Notification</h1>
        <form [formGroup]="form" (ngSubmit)="onSubmit()">

            <div mat-dialog-content fxLayout="column">

                <mat-radio-group formControlName="recipient" fxLayout="row" fxLayoutAlign="space-around center">
                    <mat-radio-button value="department"> To Departments</mat-radio-button>
                    <mat-radio-button value="employee"> To Employees</mat-radio-button>
                </mat-radio-group>

                <mat-form-field class="full-width" *ngIf="form.controls.recipient.value == 'department'">
                    <mat-label>To Departments...</mat-label>
                    <mat-select [multiple]="true" formControlName="toDepartments">
                        <mat-option *ngFor="let dep of departments$ | async" [value]="dep.id">
                            {{dep.name}}
                        </mat-option>
                    </mat-select>
                </mat-form-field>

                <mat-form-field class="full-width" *ngIf="form.controls.recipient.value == 'employee'">
                    <mat-label>To Employee</mat-label>
                    <mat-select formControlName="employees">
                        <mat-option *ngFor="let e of employees$ | async" [value]="e.id">
                            {{e.fullName}}
                        </mat-option>
                    </mat-select>
                </mat-form-field>

                <mat-form-field class="full-width">
                    <input formControlName="subject" matInput placeholder="Subject">
                </mat-form-field>

                <mat-form-field class="full-width">
                    <textarea formControlName="body" matInput placeholder="Notification Content"></textarea>
                </mat-form-field>

            </div>
            <div mat-dialog-actions>
                <button mat-button [mat-dialog-close]="null" cdkFocusInitial>Cancel</button>
                <button mat-button type="submit">Send</button>
            </div>
        </form>
    `
})
export class SendNotificationsComponent implements OnInit {
    form: FormGroup;
    departments$ = new BehaviorSubject<Department[]>([]);
    employees$ = new BehaviorSubject<User[]>([]);

    constructor(public dialogRef: MatDialogRef<SendNotificationsComponent>,
                private fb: FormBuilder,
                private notificationsDataService: NotificationsDataService
    ) {
    }

    ngOnInit() {
        this.form = this.fb.group({
            recipient: ['department'],
            subject: ['', Validators.required],
            body: ['', Validators.required],
            toDepartments: [[], Validators.required],
            employees: [[]],
        });
        this.notificationsDataService.myNotificationGrants().then(data => this.departments$.next(data));
    }

    onSubmit() {
        if (this.form.invalid) {
            return;
        }

        const notification = {
            subject: this.form.controls.subject.value,
            body: this.form.controls.body.value,
            toDepartments: this.form.controls.toDepartments.value,
        };

        this.dialogRef.close(notification);
    }

    loadEmployeesIfAllowed() {
        const deps = this.departments$.getValue().filter(d => d.canSendPersonal).map(d => d.id);
        if (deps.length) {

        }
    }
}
