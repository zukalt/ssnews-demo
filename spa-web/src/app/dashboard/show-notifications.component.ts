import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {UserNotification} from '../shared/models';


@Component({
    styles: [`
        pre {
            min-height: 200px;
            outline: none;
        }
        b {
            display: inline-block;
            width: 120px;
        }
    `],
    template: `
        <h1 mat-dialog-title>{{data.notification.subject}}</h1>

        <div mat-dialog-content fxLayout="column" fxLayoutAlign="start">
            <div><b>From:</b> {{data.notification.createdBy.fullName}}</div>
            <div><b>Department:</b> {{data.notification.createdBy.department.name}}</div>
            <div><b>Sent:</b> {{data.notification.createdAt | date:'medium'}}</div>
            <pre>{{data.notification.body}}</pre>
        </div>
        <div mat-dialog-actions>
            <button mat-button *ngIf="!data.read" [mat-dialog-close]="'read'">Mark Read</button>
            <button mat-button *ngIf="data.read" [mat-dialog-close]="'unread'">Mark UnRead</button>
            <button mat-button [mat-dialog-close]="'delete'">Delete</button>
        </div>
    `
})
export class ShowNotificationsComponent implements OnInit {


    constructor(@Inject(MAT_DIALOG_DATA) public data: UserNotification) {
    }

    ngOnInit() {

    }
}
