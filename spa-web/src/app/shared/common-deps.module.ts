import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {
    MatBadgeModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PageNotFoundComponent} from './pages';
import {MatMenuModule} from '@angular/material/menu';
import {RouterModule} from '@angular/router';
import {NotificationStatsComponent, UserInfoComponent} from './component';
import {MatSortModule} from '@angular/material/sort';

@NgModule({
    imports: [
        CommonModule,
        MatButtonModule,
        MatToolbarModule,
        MatIconModule,
        MatSidenavModule,
        MatBadgeModule,
        MatListModule,
        MatGridListModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatRadioModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatChipsModule,
        MatTooltipModule,
        MatTableModule,
        MatPaginatorModule,
        MatCardModule,
        MatSnackBarModule,
        MatTabsModule,
        MatCheckboxModule,
        MatMenuModule,
        MatSlideToggleModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        MatSortModule,
        // HttpClientModule
    ],
    exports: [
        MatButtonModule,
        MatToolbarModule,
        MatIconModule,
        MatSidenavModule,
        MatBadgeModule,
        MatListModule,
        MatGridListModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatRadioModule,
        MatDatepickerModule,
        MatChipsModule,
        MatTooltipModule,
        MatTableModule,
        MatPaginatorModule,
        MatCardModule,
        MatSnackBarModule,
        MatTabsModule,
        MatCheckboxModule,
        MatMenuModule,
        MatSlideToggleModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
        UserInfoComponent,
        NotificationStatsComponent,
    ],
    providers: [],
    declarations: [
        PageNotFoundComponent, UserInfoComponent, NotificationStatsComponent
    ]
})

export class CommonDepsModule {
}
