import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {NotificationsDataService} from '../services';
import {AdminService} from '../services/admin.service';


@Component({
    selector: 'app-notification-stats',
    styles: [`
        h3 {
            text-align: center
        }

        th.mat-sort-header-sorted {
            color: black;
        }
    `],
    template: `
        <div *ngIf="databaseNotInitialized" fxLayout="column" fxLayoutGap="5px" fxLayoutAlign="space-between center">
            <h3>For demo please upload sample-data-import.xslx</h3>

            <input type="file" (change)="handleFileInput($event.target.files)">

        </div>
        <div *ngIf="!databaseNotInitialized" class="mat-elevation-z1 mat-table" fxLayout="column" fxLayoutGap="5px"
             fxLayoutAlign="space-between stretch ">
            <h3>Notification Stats</h3>
            <table mat-table [dataSource]="dataSource" matSort>

                <ng-container matColumnDef="department">
                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Department</th>
                    <td mat-cell *matCellDef="let element"> {{element.department}} </td>
                </ng-container>
                <ng-container matColumnDef="sent">
                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Sent</th>
                    <td mat-cell *matCellDef="let element"> {{element.sent}} </td>
                </ng-container>
                <ng-container matColumnDef="received">
                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Received</th>
                    <td mat-cell *matCellDef="let element"> {{element.received}} </td>
                </ng-container>

                <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
                <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
            </table>
        </div>
    `
})
export class NotificationStatsComponent implements OnInit {
    databaseNotInitialized = false;
    dataSource = new MatTableDataSource([]);
    displayedColumns: string[] = ['department', 'sent', 'received'];

    @ViewChild(MatSort, {static: true}) sort: MatSort;

    constructor(private notificationDataService: NotificationsDataService, private adminService: AdminService) {
    }

    ngOnInit() {
        this.notificationDataService.stats()
            .then(data => {
                this.dataSource.data = data;
                this.databaseNotInitialized = data.length < 2;
            });
        this.dataSource.sort = this.sort;
    }

    handleFileInput(files: FileList) {
        this.adminService.upload(files[0]).toPromise()
            .then(
                () => {
                    this.ngOnInit();
                })
            .catch((err) => {
                alert(err);
            });

    }
}
