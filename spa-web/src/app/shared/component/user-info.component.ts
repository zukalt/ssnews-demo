import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {User} from '../models';
import {BehaviorSubject, Observable} from 'rxjs';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html'
})
export class UserInfoComponent implements OnInit, OnDestroy {

  @Input()
  user: User | Observable<User>;

  userSubject: BehaviorSubject<User> = new BehaviorSubject<User>({});

  @Input()
  canEdit = false;

  @Output()
  edit: EventEmitter<User> = new EventEmitter<User>();

  constructor() { }

  ngOnInit() {
    if ( this.user instanceof Observable) {
      this.user.subscribe(u => this.userSubject.next(u));
    } else {
      this.userSubject.next(this.user);
    }
  }

  ngOnDestroy(): void {
    this.userSubject.complete();
  }

  fireEdit() {
    this.edit.emit(this.userSubject.value);
  }
}
