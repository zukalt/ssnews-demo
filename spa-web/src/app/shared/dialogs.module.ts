import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfirmationComponent} from './dialogs';
import {CommonDepsModule} from './common-deps.module';
import {MatDialogModule} from '@angular/material/dialog';


@NgModule({
  declarations: [ConfirmationComponent],
  exports: [
  ],
  imports: [
    CommonModule,
    CommonDepsModule,
    MatDialogModule
  ]
})
export class DialogsModule { }
