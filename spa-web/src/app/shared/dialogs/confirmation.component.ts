import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface ConfirmDialogData {
  closeBtnText?: 'Close';
  okBtnText?: 'OK';
  title?: 'Confirm';
  message: '';
}

@Component({
  template: `
      <h1 mat-dialog-title>{{data.title}}</h1>
      <div mat-dialog-content>
          {{data.message}}
      </div>
      <div mat-dialog-actions>
          <button mat-button [mat-dialog-close]="false" cdkFocusInitial>{{data.closeBtnText}}</button>
          <button mat-button [mat-dialog-close]="true">{{data.okBtnText}}</button>
      </div>
  `
})
export class ConfirmationComponent implements OnInit {
  data: ConfirmDialogData;

  private defaultData: ConfirmDialogData = {
    closeBtnText: 'Close',
    okBtnText: 'OK',
    title: 'Confirm',
    message: ''
  };

  constructor(@Inject(MAT_DIALOG_DATA) data: ConfirmDialogData) {
    this.data = Object.assign(this.defaultData, data);
  }

  ngOnInit() {
  }
}
