import {Department, User} from './user.model';

export interface Notification {
    body: string;
    subject: string;
    createdAt: Date;
    createdBy: User;
}
export interface UserNotification {

    id: string;
    notification: Notification;
    read: boolean;
}
