import {User} from './user.model';

export class UserSession {
  token: string;
  user: User;
}
