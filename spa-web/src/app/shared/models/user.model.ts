export interface Department {
  id: string;
  name: string;
  canSendPersonal?: boolean;
}

export interface User {
  id?: string;
  fullName?: string;
  email?: string;
  birthDate?: string;
  department?: Department;
}

export interface SearchUserResult {
  result: User[];
  total: number;
}
