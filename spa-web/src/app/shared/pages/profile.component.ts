import {Component, OnInit} from '@angular/core';
import {AuthenticationService, NotificationService, SessionService} from '../services';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {
  formGroup: FormGroup;


  constructor(private authService: AuthenticationService,
              public  session: SessionService,
              private fb: FormBuilder,
              private notifs: NotificationService) {
  }


  ngOnInit() {
    this.formGroup = this.fb.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      newPassword2: ['', Validators.required]
    });
  }

  changePassword() {
    if (this.formGroup.invalid) {
      return;
    }
    const oldPassword = this.formGroup.controls.oldPassword.value;
    const newPassword = this.formGroup.controls.newPassword.value;
    const newPassword2 = this.formGroup.controls.newPassword2.value;
    if (newPassword !== newPassword2) {
      this.notifs.error('Passwords does not match, please retype.');
      this.resetField('newPassword2');
      this.resetField('newPassword');
      return;
    }
    this.authService.changePassword(oldPassword, newPassword)
      .toPromise().then(() => {
          this.notifs.info('Password changed');
          this.resetField('newPassword2');
          this.resetField('newPassword');
          this.resetField('oldPassword');

        }).catch(() => {
          this.notifs.warn('Old password was wrong');
          this.resetField('oldPassword');
        });
  }

  resetField(name: string, withErrors = false) {
    this.formGroup.controls[name].reset();
    if (withErrors) {
      this.formGroup.controls[name].setErrors(null);
    }
  }
}
