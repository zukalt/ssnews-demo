import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class AdminService {

    constructor(private http: HttpClient) {
    }


    upload(file: File) {
        const formData: FormData = new FormData();
        formData.append('file', file, file.name);
        return this.http
            .post('/api/system/initDatabase', formData).pipe();
    }
}
