import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

import {AuthenticationService} from './authentication.service';
import {Router} from '@angular/router';
import {NotificationService} from './notification.service';

@Injectable()
export class ApiErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService,
                private router: Router,
                private notifs: NotificationService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError((err, caught) => {
            const errData = err.error;
            if (errData.statusCode === 401 && errData.message === 'not.authenticated') {
                this.authenticationService.logout();
                this.notifs.warn('Session expired');
                this.router.navigateByUrl('/auth/login');
                return of(null);
            } else {
                return throwError(err.error.message || err.statusText);
            }
        }));
    }
}
