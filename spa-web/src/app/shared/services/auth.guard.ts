import {Injectable} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  CanLoad,
  Route,
  Router,
  RouterStateSnapshot,
  UrlSegment
} from '@angular/router';
import {Observable} from 'rxjs';
import {SessionService} from './session.service';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate, CanLoad, CanActivateChild {
  protected permissions;
  constructor(
    protected router: Router,
    protected session: SessionService
  ) {
    session.permissions$.subscribe(perms => this.permissions = perms);
  }


  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.redirectIfNotAllowed();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.redirectIfNotAllowed();
  }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return this.isAllowed();
  }

  redirectIfNotAllowed(url = '/auth/login') {
    return this.isAllowed() || this.router.createUrlTree([url]);
  }

  isAllowed() {
    return this.session.currentSession != null;
  }
}

@Injectable({providedIn: 'root'})
export class AdminGuard extends AuthGuard {
  constructor(
    router: Router,
    session: SessionService
  ) {
    super(router, session);
  }

  isAllowed(): boolean {
    return super.isAllowed() && this.permissions.isAnyAdmin;
  }

  redirectIfNotAllowed(url) {
    return super.redirectIfNotAllowed('/my');
  }

}
