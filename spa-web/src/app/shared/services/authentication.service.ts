import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {tap} from 'rxjs/operators';
// import { UserSession} from '@app/shared/models';
import {UserSession} from '../models';
import {SessionService} from './session.service';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

  constructor(private http: HttpClient, private sessionService: SessionService) {
  }

  login(email: string, password: string) {
    const payload = new HttpParams()
      .set('email', email)
      .set('password', password);

    return this.http.post<any>('/api/auth/login', payload)
      .pipe(tap((session: UserSession) => {
        this.sessionService.start(session);
      }));
  }

  private _logout() {
    this.sessionService.stop();
  }

  logout() {
    this._logout();
    this.http.get<any>('/api/auth/logout').subscribe();
  }

  register(email: string, fullName: string) {
    return this.http.post<any>('/api/auth/register', {
      email, fullName
    }).pipe();
  }

  createPassword(token: string, email: string, password: string)  {
    return this.http.post<any>('/api/auth/create-password', {
      email, token, newPassword: password
    }).pipe();
  }

  changePassword(oldPassword: string, newPassword: string) {
    return this.http.post<any>('/api/auth/change-password', {
      oldPassword, newPassword
    }).pipe();
  }

  forgotPassword(email: string) {
    return this.http.post<any>('/api/auth/forgot-password', {
      email
    }).pipe();
  }
}
