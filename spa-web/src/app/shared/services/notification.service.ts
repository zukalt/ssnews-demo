import {Component, Inject, Injectable} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBar} from '@angular/material/snack-bar';

export declare type IconType = 'info' | 'success' | 'warning' | 'done' ;
export declare type IconColorType = '' | 'primary' | 'warn' | 'accent' ;

@Injectable({ providedIn: 'root' })
export class NotificationService {

  constructor(private snackbar: MatSnackBar) {
  }

  error(error: any) {
    this.open(error.toString(), 'warning', 'warn');
  }

  warn(message: string) {
    this.open(message, 'info', 'accent');
  }

  info(message: string) {
    this.open(message, 'done');
  }

  success(message: string) {
    this.open(message, 'done', 'primary');
  }

  open(message: string, type: IconType, color: IconColorType = '') {
    this.snackbar.openFromComponent(SnackbarMessageComponent, {
      data: {icon: type, message, color}
    });
  }
}

@Component({
  template: `
      <div fxLayout="row" fxLayoutAlign="start center">
              <mat-icon fxFlex="40px" [color]="data.color">{{data.icon}}</mat-icon>
              <span >{{data.message}}</span>
      </div>
  `
})
export class SnackbarMessageComponent  {
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) {
  }
}
