import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Department, SearchUserResult, User, UserNotification} from '../models';
import {map} from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class NotificationsDataService {

  constructor(private http: HttpClient) {
  }

  search(nameLike: string, page: number, pageSize: number) {
    return this.http.post<SearchUserResult>('/api/users/search', {
      nameLike,
      page,
      pageSize
    }).pipe();
  }

  loadUser(userId: string) {
    return this.http.get<User>(`/api/users/${userId}`).pipe();
  }


  async stats() {
    const [stats, deps] = await Promise.all([
        this.http.get<any>(`/api/notifications/stats`).toPromise(),
        this.http.get<Department[]>(`/api/departments/`).toPromise()
    ]);

    return deps.map(dep => {
        return {
            department: dep.name,
            sent: stats.sentPerDepartment[dep.id] || 0,
            received: stats.receivedPerDepartment[dep.id] || 0,
        };
    });
  }

  myNotifications() {
    return this.http.get<UserNotification[]>(`/api/notifications/`).pipe();
  }

  async myNotificationGrants(): Promise<Department[]> {
      const [grants, deps] = await Promise.all([
          this.http.get<any>(`/api/notifications/grants/my`).toPromise(),
          this.http.get<Department[]>(`/api/departments/`).toPromise()
      ]);

      return deps.filter(d => grants[d.id] !== undefined).map(d => ({...d, canSendPersonal: grants[d.id]}));
  }

  allDepartments() {
    return this.http.get<Department>(`/api/departments/`).pipe();
  }

    send(data: any) {
        return this.http.post<void>('/api/notifications/send', data).pipe();
    }

    markStatus(id: string, status: boolean) {
        return this.http.put<void>(`/api/notifications/${id}`, 'true', {
            headers: {
                'Content-Type': 'application/json'
            }
        }).pipe();
    }

    deleteNotification(id: string) {
        return this.http.delete<void>(`/api/notifications/${id}`).pipe();
    }
}
