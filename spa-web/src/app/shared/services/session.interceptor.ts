import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SessionService} from './session.service';

@Injectable()
export class SessionInterceptor implements HttpInterceptor {
    constructor(private session: SessionService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add Authorization request to each request
        const session = this.session.currentSession;
        if (session && session.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: session.token
                }
            });
        }

        return next.handle(request);
    }
}
