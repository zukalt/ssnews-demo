import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {User, UserSession} from '../models';

const APP_SESSION_KEY = 'app.session';

@Injectable({ providedIn: 'root' })
export class SessionService {


  private sessionSubject: BehaviorSubject<UserSession> ;
  user$: Observable<User>;
  permissions$: Observable<Authorizations>;

  constructor() {
    this.sessionSubject = new BehaviorSubject<UserSession>(
      JSON.parse(localStorage.getItem(APP_SESSION_KEY))
    );
    this.user$ = this.sessionSubject.pipe(
      map(session => session != null ? session.user : null)
    );
    this.permissions$ = this.sessionSubject.pipe(
      map(session => session != null ? session.user : null),
      map(user => new Authorizations(user))
    );
    this.permissions$.subscribe(p => console.log(p) );
  }

  start(session: UserSession) {
    localStorage.setItem(APP_SESSION_KEY, JSON.stringify(session));
    this.sessionSubject.next(session);
  }

  stop() {
    localStorage.removeItem(APP_SESSION_KEY);
    this.sessionSubject.next(null);
  }

  get currentSession() {
    return this.sessionSubject.value;
  }

}

class Authorizations {
  blocked: boolean;
  loggedIn: boolean;
  isAnyAdmin: boolean;
  isSystemAdmin: boolean;
  isContentAdmin: boolean;
  isEditor: boolean;

  constructor(private user: User) {
    this.loggedIn = user != null;
    if (this.loggedIn) {
      this.blocked = false;
      this.isEditor = this.hasRole('EDITOR');
      this.isContentAdmin = this.hasRole('CONTENT_ADMIN');
      this.isSystemAdmin = this.hasRole('SYSTEM_ADMIN');
      this.isAnyAdmin = this.isContentAdmin || this.isSystemAdmin;
    } else {
      this.blocked = false;
      this.isEditor = false;
      this.isContentAdmin = false;
      this.isSystemAdmin = false;
      this.isAnyAdmin = false;
    }
  }
  private hasRole(role: string): boolean {
    if (!this.user) {
      return false;
    }
    return true; // this.user.roles.indexOf(role)>-1;
  }

  private hasAnyRole(roles: string[]) {
    return roles.map(r => this.hasRole(r)).indexOf(true) > -1;
  }

}
