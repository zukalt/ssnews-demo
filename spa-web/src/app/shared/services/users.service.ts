import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Department, SearchUserResult, User} from '../models';

@Injectable({ providedIn: 'root' })
export class UsersService {

  constructor(private http: HttpClient) {
  }

  search(nameLike: string, page: number, pageSize: number) {
    return this.http.post<SearchUserResult>('/api/users/search', {
      nameLike,
      page,
      pageSize
    }).pipe();
  }

  loadUser(userId: string) {
    return this.http.get<User>(`/api/users/${userId}`).pipe();
  }

  updateUser(user: User) {
    return this.http.put<any>(`/api/users/${user.id}`, user).pipe();
  }

  allDepartments() {
    return this.http.get<Department>(`/api/departments/`).pipe();
  }
}
