package com.solarsystem.app.ssnews.uc;


import com.solarsystem.app.ssnews.model.User;
import com.solarsystem.app.ssnews.model.UserCredential;
import com.solarsystem.app.ssnews.model.UserSession;
import com.solarsystem.app.ssnews.uc.common.Utils;
import com.solarsystem.app.ssnews.uc.dto.DtoMapper;
import com.solarsystem.app.ssnews.uc.dto.UserSessionDto;
import com.solarsystem.app.ssnews.uc.ex.*;
import com.solarsystem.app.ssnews.uc.ports.AppMailer;
import com.solarsystem.app.ssnews.uc.ports.PasswordEncoder;
import com.solarsystem.app.ssnews.uc.ports.rep.UserPasswordStore;
import com.solarsystem.app.ssnews.uc.ports.rep.UserSessionStore;
import com.solarsystem.app.ssnews.uc.ports.rep.UserStore;
import org.mapstruct.factory.Mappers;

import java.util.Optional;

public class Authentication {

    private PasswordEncoder passwordEncoder;
    private UserPasswordStore passwordStore;
    private UserStore userStore;
    private UserSessionStore userSessionStore;
    private AppMailer mailer;
    private int tokenValidationTimeInHours;
    private DtoMapper convert = Mappers.getMapper(DtoMapper.class);

    public Authentication(PasswordEncoder passwordEncoder,
                          UserPasswordStore passwordStore,
                          UserStore userStore,
                          UserSessionStore userSessionStore,
                          AppMailer mailer,
                          int tokenValidationTimeInHours) {
        this.passwordEncoder = passwordEncoder;
        this.passwordStore = passwordStore;
        this.userStore = userStore;
        this.userSessionStore = userSessionStore;
        this.mailer = mailer;
        this.tokenValidationTimeInHours = tokenValidationTimeInHours;
    }

    public UserSessionDto passwordLogin(String email, String password) throws NotAuthorizedException {
        return passwordStore.getByEmail(email)
                .filter(uc -> passwordEncoder.matches(uc.getPasswordHash(), password))
                .map(uc -> userStore.findByEmail(email))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(UserSession::new)
                .map(session -> userSessionStore.save(session))
                .map(convert::toSessionDto)
                .orElseThrow(NotAuthorizedException::new);
    }

    UserCredential createPasswordToken(User user) {
        return createOrResetPasswordResetToken(user);
    }

    public void requestPasswordReset(String email) throws UnsuccessfulOperationException {
        Optional<User> user = userStore.findByEmail(email);
        if (user.isPresent()) {
            UserCredential userCredential = createOrResetPasswordResetToken(user.get());
            mailer.sendPasswordResetEmail(user.get(), userCredential);
        }
    }

    public void resetPassword(String email, String token, String newPassword) throws PasswordResetFailedException {
        UserCredential credential = passwordStore
                .getByEmail(email)
                .filter(uc -> token.equals(uc.getPasswordResetToken()))
                .orElseThrow(PasswordResetFailedException::new);

        credential.setTokenValidTill(null);
        credential.setPasswordResetToken(null);
        credential.setPasswordHash(passwordEncoder.encode(newPassword));
        passwordStore.save(credential);
    }

    public void logout() {
        try {
            userSessionStore.invalidate(AuthorizationContext.currentSession().getToken());
        } catch (NotAuthorizedException e) {
            // if there is no session, then keep quiet
        }
    }

    public void changePassword(String oldPassword, String newPassword) throws NotAuthorizedException, UserNotFoundException {
        String email = AuthorizationContext.currentSession().getUser().getEmail();
        UserCredential userCredential = passwordStore
                .getByEmail(email)
                .orElseThrow(UserNotFoundException::new);

        if (passwordEncoder.matches(userCredential.getPasswordHash(), oldPassword)) {
            userCredential.setPasswordHash(passwordEncoder.encode(newPassword));
            passwordStore.save(userCredential);
        } else {
            throw new NotAuthenticatedException();
        }
    }

    private UserCredential createOrResetPasswordResetToken(User user) {
        UserCredential userCredential = passwordStore
                .getByEmail(user.getEmail())
                .orElse(new UserCredential(user.getEmail()));

        userCredential.setPasswordResetToken(Utils.randomUUID());
        userCredential.setTokenValidTill(Utils.timeAfterHours(tokenValidationTimeInHours));

        passwordStore.save(userCredential);
        return userCredential;
    }
}
