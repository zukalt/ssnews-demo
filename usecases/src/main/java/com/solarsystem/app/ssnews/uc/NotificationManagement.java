package com.solarsystem.app.ssnews.uc;

import com.solarsystem.app.ssnews.model.*;
import com.solarsystem.app.ssnews.uc.common.Utils;
import com.solarsystem.app.ssnews.uc.dto.DtoMapper;
import com.solarsystem.app.ssnews.uc.dto.NotificationGrantDto;
import com.solarsystem.app.ssnews.uc.dto.NotificationStats;
import com.solarsystem.app.ssnews.uc.dto.UserNotificationDto;
import com.solarsystem.app.ssnews.uc.ex.NotAuthorizedException;
import com.solarsystem.app.ssnews.uc.ex.UserNotFoundException;
import com.solarsystem.app.ssnews.uc.ports.PermissionProvider;
import com.solarsystem.app.ssnews.uc.ports.rep.NotificationGrantStore;
import com.solarsystem.app.ssnews.uc.ports.rep.NotificationStore;
import com.solarsystem.app.ssnews.uc.ports.rep.UserDepartmentStore;
import com.solarsystem.app.ssnews.uc.ports.rep.UserStore;
import org.mapstruct.factory.Mappers;

import java.util.*;
import java.util.stream.Collectors;

public class NotificationManagement {

    private PermissionProvider grants;
    private NotificationStore notificationStore;
    private NotificationGrantStore grantsStore;
    private UserStore userStore;
    private UserDepartmentStore departmentStore;
    private DtoMapper dtoMapper = Mappers.getMapper(DtoMapper.class);

    public NotificationManagement(PermissionProvider grants,
                                  NotificationStore notificationStore,
                                  NotificationGrantStore notificationGrantStore,
                                  UserStore userStore,
                                  UserDepartmentStore departmentStore) {
        this.grants = grants;
        this.notificationStore = notificationStore;
        this.grantsStore = notificationGrantStore;
        this.userStore = userStore;
        this.departmentStore = departmentStore;
    }

    public void addNotificationGrant(NotificationGrantDto grantDto) throws NotAuthorizedException {
        grants.authorize(NotificationManagement.class, "addNotificationGrant");
        Optional<NotificationGrant> optionalGrant = grantsStore.findGrant(
                dtoMapper.department(grantDto.fromDepartment),
                dtoMapper.department(grantDto.toDepartment)
        );

        NotificationGrant grant = optionalGrant
                .map(g -> {
                    // update if one exists
                    g.setPersonalNotificationsAllowed(grantDto.personalNotificationsAllowed);
                    return g;
                })
                .orElseGet(() -> {
                    // ... or create new one
                    grantDto.id = Utils.randomUUID();
                    return dtoMapper.notificationGrant(grantDto);
                });

        grantsStore.save(grant);
    }

    public void removeNotificationGrantById(String id) throws NotAuthorizedException {
        grants.authorize(NotificationManagement.class, "removeNotificationGrant");
        grantsStore.deleteById(id);
    }

    public List<NotificationGrantDto> listAllGrants() throws NotAuthorizedException {
        grants.authorize(NotificationManagement.class, "listAllGrants");
        return grantsStore.findAll().stream().map(dtoMapper::notificationGrant).collect(Collectors.toList());
    }

    public Map<String, Boolean> listCurrentUserGrants() throws NotAuthorizedException {
        Department userDepartment = AuthorizationContext.currentSession().getUser().getDepartment();
        return grantsStore.findAll()
                .stream()
                .filter(grant -> grant.getFromDepartment().getId().equals(userDepartment.getId()))
                .collect(Collectors.toMap(
                        key -> key.getToDepartment().getId(),
                        value -> value.isPersonalNotificationsAllowed()
                ));
    }

    public void sendNotification(Set<String> toDepartments, String subject, String body) throws NotAuthorizedException {
        Map<String, Boolean> allowedDestinations = listCurrentUserGrants();
        if (!allowedDestinations.keySet().containsAll(toDepartments)) {
            throw new NotAuthorizedException();
        }
        Notification notification = new Notification();
        notification.setId(Utils.randomUUID());
        notification.setSubject(subject);
        notification.setBody(body);
        notification.setCreatedBy(AuthorizationContext.currentSession().getUser());
        notification.setCreatedAt(new Date());
        notificationStore.save(notification);

        final String senderDepartment = AuthorizationContext.currentSession().getUser().getDepartment().getId();

        for (String id : toDepartments) {
            departmentStore.findById(id).ifPresent(department -> {
                department.getEmployees().forEach(user -> {
                    if (!user.getIgnoreList().contains(senderDepartment)) {
                        notificationStore.saveForUser(user, notification);
                    }
                });
            });
        }
    }

    public void sendPersonalNotification(String userId, String subject, String body)
            throws NotAuthorizedException, UserNotFoundException {

        User sender = AuthorizationContext.currentSession().getUser();
        User targetUser = userStore.findById(userId).orElseThrow(UserNotFoundException::new);

        Optional<NotificationGrant> grant = grantsStore.findGrant(sender.getDepartment(), targetUser.getDepartment());
        boolean canSendPersonal = grant.orElseThrow(NotAuthorizedException::new).isPersonalNotificationsAllowed();

        if (!canSendPersonal) {
            throw new NotAuthorizedException();
        }

        Notification notification = new Notification();
        notification.setId(Utils.randomUUID());
        notification.setSubject(subject);
        notification.setBody(body);
        notification.setCreatedBy(sender);
        notification.setCreatedAt(new Date());
        notificationStore.save(notification);

        notificationStore.saveForUser(targetUser, notification);
    }

    public List<UserNotificationDto> listCurrentUserNotifications() throws NotAuthorizedException {
        User currentUser = AuthorizationContext.currentSession().getUser();
        List<UserNotification> notifications = notificationStore.getUserNotifications(currentUser);
        return dtoMapper.userNotifications(notifications);
    }

    public void removeUserNotification(String userNotificationId) throws NotAuthorizedException {
        User currentUser = AuthorizationContext.currentSession().getUser();
        notificationStore.removeUserNotification(currentUser, userNotificationId);
    }

    public void updateUserNotificationStatus(String userNotificationId, boolean status) throws NotAuthorizedException {
        User currentUser = AuthorizationContext.currentSession().getUser();
        notificationStore.updateUserNotificationStatus(currentUser, userNotificationId, status);
    }

    public NotificationStats notificationStats() throws NotAuthorizedException {
        grants.authorize(NotificationManagement.class, "notificationStats");
        return notificationStore.collectStatsPerDepartment();
    }
}
