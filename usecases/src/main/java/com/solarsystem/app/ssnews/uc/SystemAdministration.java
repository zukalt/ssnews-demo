package com.solarsystem.app.ssnews.uc;

import com.solarsystem.app.ssnews.uc.dto.DepartmentDto;
import com.solarsystem.app.ssnews.uc.dto.NotificationGrantDto;
import com.solarsystem.app.ssnews.uc.dto.UserDto;
import com.solarsystem.app.ssnews.uc.ex.NotAuthorizedException;
import com.solarsystem.app.ssnews.uc.ex.UnsuccessfulOperationException;
import com.solarsystem.app.ssnews.uc.ports.OrgDataExcelReader;
import com.solarsystem.app.ssnews.uc.ports.PermissionProvider;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;


public class SystemAdministration {

    private UserAdministration userAdministration;
    private NotificationManagement notificationManagement;
    private OrgDataExcelReader excelReader;
    private PermissionProvider grants;


    public SystemAdministration(UserAdministration userAdministration,
                                NotificationManagement notificationManagement,
                                OrgDataExcelReader excelReader,
                                PermissionProvider grants){
        this.userAdministration = userAdministration;
        this.notificationManagement = notificationManagement;
        this.excelReader = excelReader;
        this.grants = grants;
    }

    public Collection<String> importAppDataFromExcel(InputStream excelInput) throws UnsuccessfulOperationException, NotAuthorizedException {
        grants.authorize(SystemAdministration.class, "importAppDataFromExcel");

        if (userAdministration.listAllDepartment().size() > 1) {
            throw new UnsuccessfulOperationException("Database is already initialized");
        }

        OrgDataExcelReader.ReadResult excelData;
        try {
            excelData = excelReader.parse(excelInput);
        } catch (IOException e) {
            throw new UnsuccessfulOperationException(e.getMessage());
        }

        for (DepartmentDto departmentDto : excelData.departmentList) {
            userAdministration.addDepartment(departmentDto);
        }

        for(UserDto user: excelData.userList) {
            userAdministration.addNewUser(user);
        }

        for(NotificationGrantDto grantDto: excelData.notificationGrants){
            notificationManagement.addNotificationGrant(grantDto);
        }

        return excelData.errors;
    }
}
