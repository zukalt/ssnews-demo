package com.solarsystem.app.ssnews.uc;

import com.solarsystem.app.ssnews.model.Department;
import com.solarsystem.app.ssnews.model.User;
import com.solarsystem.app.ssnews.model.UserCredential;
import com.solarsystem.app.ssnews.uc.common.Utils;
import com.solarsystem.app.ssnews.uc.dto.*;
import com.solarsystem.app.ssnews.uc.ex.*;
import com.solarsystem.app.ssnews.uc.ports.AppMailer;
import com.solarsystem.app.ssnews.uc.ports.PermissionProvider;
import com.solarsystem.app.ssnews.uc.ports.rep.UserDepartmentStore;
import com.solarsystem.app.ssnews.uc.ports.rep.UserStore;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class UserAdministration {

    private UserStore userStore;
    private AppMailer mailer;
    private Authentication authService;
    private UserDepartmentStore departments;
    private PermissionProvider grants;
    private DtoMapper dtoMapper = Mappers.getMapper(DtoMapper.class);

    public UserAdministration(UserStore userStore,
                              AppMailer mailer,
                              Authentication authService,
                              UserDepartmentStore departments,
                              PermissionProvider grants) {
        this.userStore = userStore;
        this.mailer = mailer;
        this.authService = authService;
        this.departments = departments;
        this.grants = grants;
    }

    public UserIdentityDto addAndInviteNewUser(UserDto userData) throws UnsuccessfulOperationException, NotAuthorizedException {
        User user = saveNewUser(userData);
        UserCredential credential = authService.createPasswordToken(user);
        mailer.sendUserActivationEmail(user, credential);
        return dtoMapper.userIdentity(user);
    }

    public void addNewUser(UserDto userData) throws NotAuthorizedException, UserAlreadyExistsException {
        saveNewUser(userData);
    }

    public List<UserIdentityDto> listAllUsers() throws NotAuthorizedException {
        grants.authorize(UserAdministration.class, "listAllUsers");
        return userStore.findAll().stream()
                .map(dtoMapper::userIdentity)
                .collect(Collectors.toList());
    }

    public SearchResult<UserIdentityDto> searchUsers(String nameLike, int page, int pageSize) throws NotAuthorizedException {
        grants.authorize(UserAdministration.class, "searchUsers");
        SearchResult<User> users = userStore.findUsersByFullName(nameLike, page, pageSize);
        List<UserIdentityDto> userDtos = users.getResult().stream().map(dtoMapper::userIdentity).collect(Collectors.toList());
        return new SearchResult<>(userDtos, users.getTotal());
    }

    public UserDto getUser(String id) throws NotAuthorizedException, UserNotFoundException {
        grants.authorize(UserAdministration.class, "getUser");
        return dtoMapper.user(userStore.findById(id).orElseThrow(UserNotFoundException::new));

    }

    public void updateUserNameAndDepartment(String id, String fullName, String department)
            throws NotAuthorizedException, UserNotFoundException {

        grants.authorize(UserAdministration.class, "updateUser");
        User user = userStore.findById(id).orElseThrow(UserNotFoundException::new);
        user.setFullName(fullName);
        user.setDepartment(new Department(department));
        userStore.save(user);
    }

    public void updateCurrentUserIgnoreList(Set<String> ignoreList) throws NotAuthorizedException {
        User currentUser = AuthorizationContext.currentSession().getUser();
        User dbUser = userStore.findById(currentUser.getId()).orElseThrow(NotAuthorizedException::new);
        dbUser.setIgnoreList(ignoreList);
        userStore.save(dbUser);
    }

    public Set<String> getCurrentUserIgnoreList() throws NotAuthorizedException {
        User currentUser = AuthorizationContext.currentSession().getUser();
        User dbUser = userStore.findById(currentUser.getId()).orElseThrow(NotAuthorizedException::new);
        return dbUser.getIgnoreList();
    }

    public List<DepartmentDto> listAllDepartment() throws NotAuthorizedException {
        grants.authorize(UserAdministration.class, "listAllDepartment");
        return departments.findAll().stream().map(dtoMapper::department).collect(Collectors.toList());
    }

    public void addDepartment(DepartmentDto departmentDto)
            throws NotAuthorizedException, UnsuccessfulOperationException {

        grants.authorize(UserAdministration.class, "addDepartment");
        if (departments.findById(departmentDto.id).isPresent()){
            throw new UnsuccessfulOperationException("Department already exists");
        }
        departments.save(dtoMapper.department(departmentDto));
    }

    public DepartmentDto updateDepartment(String id, DepartmentDto departmentDto)
            throws DepartmentNotFoundException, NotAuthorizedException {

        grants.authorize(UserAdministration.class, "updateDepartment");
        Department dep = departments.findById(id).orElseThrow(DepartmentNotFoundException::new);
        dep.setName(departmentDto.name);
        departments.save(dep);
        return departmentDto;
    }

    private User saveNewUser(UserDto userData) throws NotAuthorizedException, UserAlreadyExistsException {
        grants.authorize(UserAdministration.class, "addUser", userData.department);
        if (userStore.findByEmail(userData.email).isPresent()){
            throw new UserAlreadyExistsException();
        }
        User user = new User();
        user.setId(Utils.randomUUID());
        user.setEmail(userData.email);
        user.setFullName(userData.fullName);
        user.setBirthDate(userData.birthDate);
        user.setDepartment(dtoMapper.department(userData.department));
        userStore.save(user);
        return user;
    }
}
