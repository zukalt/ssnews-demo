package com.solarsystem.app.ssnews.uc.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class DepartmentDto {
    public String id, name;
}
