package com.solarsystem.app.ssnews.uc.dto;


import com.solarsystem.app.ssnews.model.*;
import org.mapstruct.*;

import java.util.List;


@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class DtoMapper  {

    // User vs UserDto vs UserIdentityDto
    public abstract UserDto user(User user);
    @Named("userIdentity")
    public abstract UserIdentityDto userIdentity(User user);
    @Named("userFull")
    public abstract User user(UserDto user);

    // Department vs DepartmentDto vs String
    @Mapping(target = "employees", ignore = true)
    public abstract Department department(DepartmentDto department) ;
    public abstract DepartmentDto department(Department department) ;
    public Department department(String department) {
        return new Department(department);
    }

    // NotificationGrant vs NotificationGrantDto
    public abstract NotificationGrant notificationGrant(NotificationGrantDto grant);
    @Mappings({
            @Mapping(target = "fromDepartment", source = "fromDepartment.id"),
            @Mapping(target = "toDepartment", source = "toDepartment.id")
    })
    public abstract NotificationGrantDto notificationGrant(NotificationGrant grant);

    @Mappings({
            @Mapping(target = "createdBy", qualifiedByName = "userIdentity")
    })
    public abstract NotificationDto notification(Notification notification);
    public abstract List<UserNotificationDto> userNotifications(List<UserNotification> notifications);

    // UserSession vs UserSessionDto
    public abstract UserSessionDto toSessionDto(UserSession session);
}
