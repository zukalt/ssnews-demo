package com.solarsystem.app.ssnews.uc.dto;

import java.time.LocalDateTime;

public class NotificationDto {

    public String subject;
    public String body;
    public UserIdentityDto createdBy;
    public LocalDateTime createdAt;

}
