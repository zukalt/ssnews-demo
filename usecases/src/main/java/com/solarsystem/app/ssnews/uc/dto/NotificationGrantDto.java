package com.solarsystem.app.ssnews.uc.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class NotificationGrantDto {

    public String id;
    public String fromDepartment;
    public String toDepartment;
    public boolean personalNotificationsAllowed;
}
