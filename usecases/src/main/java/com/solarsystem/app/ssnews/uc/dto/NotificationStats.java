package com.solarsystem.app.ssnews.uc.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
public class NotificationStats {
    public Map<String, Long> sentPerDepartment;
    public Map<String, Long> receivedPerDepartment;
}
