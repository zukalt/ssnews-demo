package com.solarsystem.app.ssnews.uc.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class SearchResult<T> {
    private List<T> result;
    private long total;
}
