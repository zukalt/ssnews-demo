package com.solarsystem.app.ssnews.uc.dto;

import java.time.LocalDate;
import java.util.Set;

public class UserDto extends UserIdentityDto{
    public LocalDate birthDate;
    public Set<String> ignoreList;
}
