package com.solarsystem.app.ssnews.uc.dto;

public class UserIdentityDto {
    public String id, email, fullName;
    public DepartmentDto department;
}
