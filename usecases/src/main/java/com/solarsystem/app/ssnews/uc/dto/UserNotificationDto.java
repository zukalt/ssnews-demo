package com.solarsystem.app.ssnews.uc.dto;

public class UserNotificationDto {
    public String id;
    public NotificationDto notification;
    public boolean read;
}
