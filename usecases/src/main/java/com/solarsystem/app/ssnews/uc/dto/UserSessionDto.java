package com.solarsystem.app.ssnews.uc.dto;

public class UserSessionDto {
    public String token;
    public UserDto user;
}
