package com.solarsystem.app.ssnews.uc.ex;


import com.solarsystem.app.ssnews.uc.common.Utils;

public class AppBaseException extends Exception{

    public String getMessage() {
        return Utils.exceptionClassToMessageCode(getClass());
    }
}
