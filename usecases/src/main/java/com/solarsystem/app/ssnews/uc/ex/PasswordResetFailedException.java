package com.solarsystem.app.ssnews.uc.ex;

public class PasswordResetFailedException extends UnsuccessfulOperationException {
    public PasswordResetFailedException() {
        super("password.reset.failed");
    }
}
