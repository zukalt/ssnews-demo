package com.solarsystem.app.ssnews.uc.ex;

public class UnsuccessfulOperationException extends AppBaseException {
    private String message;

    public UnsuccessfulOperationException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
