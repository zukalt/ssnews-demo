package com.solarsystem.app.ssnews.uc.ex;

public class UserAlreadyExistsException extends UnsuccessfulOperationException {
    public UserAlreadyExistsException() {
        super("user.already.exists");
    }
}
