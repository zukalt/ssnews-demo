package com.solarsystem.app.ssnews.uc.impl;

import com.solarsystem.app.ssnews.model.Department;
import com.solarsystem.app.ssnews.uc.AuthorizationContext;
import com.solarsystem.app.ssnews.uc.NotificationManagement;
import com.solarsystem.app.ssnews.uc.SystemAdministration;
import com.solarsystem.app.ssnews.uc.UserAdministration;
import com.solarsystem.app.ssnews.uc.ex.NotAuthorizedException;
import com.solarsystem.app.ssnews.uc.ports.PermissionProvider;

public class DefaultPermissionProvider implements PermissionProvider {


    @Override
    public void authorize(Class<?> clazz, String action, Object... input) throws NotAuthorizedException {
        if (SystemAdministration.class.isAssignableFrom(clazz)) {
            verifyFromOneOfDepartments(Department.SYSTEM_DEPARTMENT.getId());
        }
        else if (UserAdministration.class.isAssignableFrom(clazz)) {
//            verifyFromOneOfDepartments(Department.SYSTEM_DEPARTMENT.getId());
        }
        else if (NotificationManagement.class.isAssignableFrom(clazz)) {
//            verifyFromOneOfDepartments(Department.SYSTEM_DEPARTMENT.getId());
            // TODO implement real one
        }
        else {
            System.err.println("WARNING: Authorization not configured for class: " + clazz.getSimpleName()+", action: "+action);
            throw new NotAuthorizedException();
        }

    }

    void verifyFromOneOfDepartments(String...deps) throws NotAuthorizedException {
        String userDep = AuthorizationContext.getUserSession().user.department.id;
        for (String dep: deps){
            if (dep.equals(userDep)) {
                return;
            }
        }
        throw new NotAuthorizedException();
    }
}
