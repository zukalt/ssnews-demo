package com.solarsystem.app.ssnews.uc.ports;


import com.solarsystem.app.ssnews.model.User;
import com.solarsystem.app.ssnews.model.UserCredential;
import com.solarsystem.app.ssnews.uc.ex.UnsuccessfulOperationException;

public interface AppMailer {

    void sendUserActivationEmail(User user, UserCredential activationToken) throws UnsuccessfulOperationException;

    void sendPasswordResetEmail(User user, UserCredential uc) throws UnsuccessfulOperationException;
}
