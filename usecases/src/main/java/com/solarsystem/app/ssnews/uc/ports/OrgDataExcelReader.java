package com.solarsystem.app.ssnews.uc.ports;

import com.solarsystem.app.ssnews.uc.dto.DepartmentDto;
import com.solarsystem.app.ssnews.uc.dto.NotificationGrantDto;
import com.solarsystem.app.ssnews.uc.dto.UserDto;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;


public interface OrgDataExcelReader {

    class ReadResult {
        public final Collection<DepartmentDto> departmentList;
        public final Collection<UserDto> userList;
        public final Collection<NotificationGrantDto> notificationGrants;
        public final Collection<String> errors;

        public ReadResult(Collection<DepartmentDto> departmentList,
                          Collection<UserDto> userList,
                          Collection<NotificationGrantDto> notificationGrants,
                          Collection<String> errors) {
            this.departmentList = departmentList;
            this.userList = userList;
            this.notificationGrants = notificationGrants;
            this.errors = errors;
        }
    }

    ReadResult parse(InputStream excelInput) throws IOException;
}
