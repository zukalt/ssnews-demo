package com.solarsystem.app.ssnews.uc.ports;

public interface PasswordEncoder {

    String encode(String password) ;
    boolean matches(String encodedPass, String providedPass) ;
}
