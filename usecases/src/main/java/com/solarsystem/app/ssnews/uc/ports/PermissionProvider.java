package com.solarsystem.app.ssnews.uc.ports;

import com.solarsystem.app.ssnews.uc.ex.NotAuthorizedException;

public interface PermissionProvider {

    void authorize(Class<?> clazz, String action, Object ...input) throws NotAuthorizedException;
}
