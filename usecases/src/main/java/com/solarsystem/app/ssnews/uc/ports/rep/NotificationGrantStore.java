package com.solarsystem.app.ssnews.uc.ports.rep;

import com.solarsystem.app.ssnews.model.Department;
import com.solarsystem.app.ssnews.model.NotificationGrant;

import java.util.List;
import java.util.Optional;

public interface NotificationGrantStore {

    NotificationGrant save(NotificationGrant grant);
    void deleteById(String id);
    List<NotificationGrant> findAll();
    Optional<NotificationGrant> findGrant(Department fromDepartment, Department toDepartment);
}
