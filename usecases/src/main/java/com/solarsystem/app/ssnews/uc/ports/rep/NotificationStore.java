package com.solarsystem.app.ssnews.uc.ports.rep;

import com.solarsystem.app.ssnews.model.Notification;
import com.solarsystem.app.ssnews.model.User;
import com.solarsystem.app.ssnews.model.UserNotification;
import com.solarsystem.app.ssnews.uc.dto.NotificationStats;

import java.util.List;

public interface NotificationStore {

    Notification save(Notification notification);
    void saveForUser(User user, Notification notification);

    List<UserNotification> getUserNotifications(User currentUser);

    void removeUserNotification(User currentUser, String userNotificationId);

    void updateUserNotificationStatus(User currentUser, String userNotificationId, boolean read);

    NotificationStats collectStatsPerDepartment();
}
