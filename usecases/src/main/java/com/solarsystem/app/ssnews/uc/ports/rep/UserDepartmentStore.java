package com.solarsystem.app.ssnews.uc.ports.rep;

import com.solarsystem.app.ssnews.model.Department;

import java.util.List;
import java.util.Optional;

public interface UserDepartmentStore {

    Department save(Department department);
    Optional<Department> findById(String id);
    List<Department> findAll();
}
