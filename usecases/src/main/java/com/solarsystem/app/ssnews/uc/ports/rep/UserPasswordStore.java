package com.solarsystem.app.ssnews.uc.ports.rep;


import com.solarsystem.app.ssnews.model.UserCredential;

import java.util.Optional;

public interface UserPasswordStore {

    Optional<UserCredential> getByEmail(String email) ;
    void save(UserCredential userCredential) ;
}
