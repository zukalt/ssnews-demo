package com.solarsystem.app.ssnews.uc.ports.rep;


import com.solarsystem.app.ssnews.model.UserSession;

import java.util.Optional;

public interface UserSessionStore {

    UserSession save(UserSession session) ;
    void invalidate(String sessionToken) ;
    Optional<UserSession> findByToken(String token) ;
    void cleanExpiredSessions();
}
