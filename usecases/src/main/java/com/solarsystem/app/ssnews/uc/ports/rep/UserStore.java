package com.solarsystem.app.ssnews.uc.ports.rep;


import com.solarsystem.app.ssnews.model.User;
import com.solarsystem.app.ssnews.uc.dto.SearchResult;

import java.util.List;
import java.util.Optional;

public interface UserStore {
    Optional<User> findByEmail(String email) ;
    List<User> findAll() ;
    void save(User user);
    SearchResult<User> findUsersByFullName(String nameLike, int page, int pageSize);
    Optional<User> findById(String id);
}
