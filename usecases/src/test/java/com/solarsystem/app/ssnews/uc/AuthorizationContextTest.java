package com.solarsystem.app.ssnews.uc;

import com.solarsystem.app.ssnews.model.UserSession;
import com.solarsystem.app.ssnews.uc.ex.AppBaseException;
import com.solarsystem.app.ssnews.uc.ex.NotAuthorizedException;
import com.solarsystem.app.ssnews.uc.impl.SessionInMemoryStoreImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class AuthorizationContextTest {
    private SessionInMemoryStoreImpl sessionStore = new SessionInMemoryStoreImpl(1000) ;
    private AuthorizationContext authorizationContext = new AuthorizationContext(sessionStore);
    @Before
    public void before() {

    }

    @Test
    public void test() throws AppBaseException {
        UserSession session = new UserSession(null);
        session = sessionStore.save(session) ;

        // usage with Supplier
        UserSession returnedBySupplier = authorizationContext.runInContext(session.getToken(), AuthorizationContext::currentSession);

        assertEquals(session.getToken(), returnedBySupplier.getToken());

        // usage with Consumer
        authorizationContext.runInContext("not-existing-token", ()-> {
            try {
                AuthorizationContext.currentSession();
                fail("Should throw NotAuthorizedException if there is no such session");
            }
            catch (NotAuthorizedException ex) {
                // ok
            }
        });
    }


}
