package com.solarsystem.app.ssnews.uc.common;

import com.solarsystem.app.ssnews.uc.ex.UserNotFoundException;
import org.junit.Test;

import static com.solarsystem.app.ssnews.uc.common.Utils.exceptionClassNameToMessageCode;
import static org.junit.Assert.assertEquals;

public class UtilsTest {

    @Test
    public void testExceptionClassNameToMessageCode() {
        assertEquals("a.b.c", exceptionClassNameToMessageCode("ABC"));
        assertEquals( "user.not.found", exceptionClassNameToMessageCode("UserNotFoundException"));
        assertEquals( "user.not.found", exceptionClassNameToMessageCode(UserNotFoundException.class.getSimpleName()));
    }
}
